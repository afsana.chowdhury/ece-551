#include <math.h>

#include <cstdio>
#include <cstdlib>
#include <iostream>

#include "function.h"
int binarySearchForZero(Function<int, int> * f, int low, int high);

class LinearFunc : public Function<int, int> {
 public:
  virtual int invoke(int arg) { return arg; }
};

class SinFunction : public Function<int, int> {
 public:
  virtual int invoke(int arg) { return 10000000 * (sin(arg / 100000.0) - 0.5); }
};

class CountedIntFn : public Function<int, int> {
 protected:
  unsigned remaining;
  Function<int, int> * f;
  const char * mesg;

 public:
  CountedIntFn(unsigned n, Function<int, int> * fn, const char * m) :
      remaining(n),
      f(fn),
      mesg(m) {}
  virtual int invoke(int arg) {
    if (remaining == 0) {
      fprintf(stderr, "Too many function invocations in %s\n", mesg);
      exit(EXIT_FAILURE);
    }
    remaining--;
    return f->invoke(arg);
  }
};

void check(Function<int, int> * f,
           int low,
           int high,
           int expected_ans,
           const char * mesg) {
  int maxInvocation;
  if (low < high) {
    maxInvocation = log2(high - low) + 1;
  }
  else {
    maxInvocation = 1;
  }

  CountedIntFn * CountInv = new CountedIntFn(maxInvocation, f, mesg);
  int ans = binarySearchForZero(CountInv, low, high);
  if (ans != expected_ans) {
    fprintf(stderr, "Error in test: %s\n", mesg);
    exit(EXIT_FAILURE);
  }
  delete CountInv;
}

int main(void) {
  LinearFunc Linearf;
  check(&Linearf, 1, 6, 1, "all positive\n");
  check(&Linearf, -2, 4, 0, "normal\n");
  check(&Linearf, -4, -1, -2, "all negative\n");
  check(&Linearf, 1, 10, 1, "All positive\n");
  check(&Linearf, -10, -1, -2, "All negative (case-1)\n");
  check(&Linearf, -10, 0, -1, "All negative (case-2)\n");
  check(&Linearf, -10, 10, 0, "negative and positive\n");
  check(&Linearf, 0, 0, 0, "Zeros\n");

  SinFunction SineF;
  check(&SineF, 0, 150000, 52359, "Provided sine func!\n");
  return EXIT_SUCCESS;
}
