#include "rand_story.h"

int main(int argc, char ** argv) {
  if (argc != 2) {
    fprintf(stderr, "Error: Incorrect number of arguments!\n");
    exit(EXIT_FAILURE);
  }
  FILE * inputFile = openFileToRead(argv[1]);

  lines_t * inputLinesArr = createLinesArray();
  readAndSaveLines(inputFile, inputLinesArr);
  if (!closeFile(inputFile)) {
    freeReadLines(inputLinesArr);
    exit(EXIT_FAILURE);
  }

  replaceBlanks(inputLinesArr, NULL, 0);

  printReadLines(inputLinesArr);
  freeReadLines(inputLinesArr);

  return EXIT_SUCCESS;
}
