#ifndef __RAND_STORY_H__
#define __RAND_STORY_H__

#include <stdio.h>
#include <string.h>

#include "provided.h"

// struct to store read lines from the input story template file
struct lines_tag {
  char ** lines;
  size_t n_lines;
};
typedef struct lines_tag lines_t;

struct used_tag {
  char ** words;
  size_t n_words;
};
typedef struct used_tag used_t;

// Function prototypes
FILE * openFileToRead(char * filename);
int closeFile(FILE * filename);

// step-1
lines_t * createLinesArray();
void readAndSaveLines(FILE * inputFile, lines_t * inputLinesArr);
void printReadLines(lines_t * linesArr);
void replaceBlanks(lines_t * linesArr, catarray_t * cats, int option_n);
int replaceBlankOneLine(char ** line,
                        catarray_t * cats,
                        used_t * usedWords,
                        int option_n);
char * mergeSubStrings(char * substrBeforeBlank,
                       char * replacedWord,
                       char * substrAfterBlank);
void freeReadLines(lines_t * linesArr);

// step-2
catarray_t * createCategoryArray();
void readAndSaveWords(FILE * inputFile, catarray_t * categoryArr);
void removeNewLineChar(char * currentline);
void addCategoryWord(char * currentline, char * colon, catarray_t * categoryArr);
category_t * addCategory(char * category, catarray_t * categoryArr);
category_t * addNewCategory(char * category, catarray_t * categoryArr);
category_t * lookUpCategory(char * category, catarray_t * categoryArr);
void addWord(char * word, category_t * category);
void freeCategoryWords(catarray_t * categoryArr);

// step-3
char * chooseWord_helper(char * category,
                         catarray_t * cats,
                         used_t * usedWords,
                         int option_n);
used_t * createUsedCat();
void addUsedWord(const char * word, used_t * usedWords);
size_t isValidInt(char * string, size_t n_words);
const char * findUsed(used_t * usedWords, size_t offset);
void freeUsedWords(used_t * usedWords);

// step-4
int checkArgOption(char * option);
void removeWord(const char * word, category_t * catInArr);
void moveArrUp(category_t * catInArr, size_t start_index);
#endif
