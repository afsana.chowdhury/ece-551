#include "rand_story.h"

int main(int argc, char ** argv) {
  char * catFileName = NULL;
  char * storyFileName = NULL;
  int option_n = 0;
  if (argc == 3) {  // no option
    catFileName = argv[1];
    storyFileName = argv[2];
  }
  else if (argc == 4) {
    option_n = checkArgOption(argv[1]);
    catFileName = argv[2];
    storyFileName = argv[3];
  }
  else {
    fprintf(stderr, "Error: Incorrect number of arguments!\n");
    exit(EXIT_FAILURE);
  }

  FILE * catFile = openFileToRead(catFileName);
  FILE * storyFile = fopen(storyFileName, "r");
  if (storyFile == NULL) {
    fprintf(stderr, "Error: Couldn't open file %s\n", storyFileName);
    fclose(catFile);
    exit(EXIT_FAILURE);
  }
  // parse the category file first
  catarray_t * categoryArr = createCategoryArray();
  readAndSaveWords(catFile, categoryArr);
  if (!closeFile(catFile)) {
    freeCategoryWords(categoryArr);
    exit(EXIT_FAILURE);
  }

  // now parse the template and replace blanks
  lines_t * inputLinesArr = createLinesArray();
  readAndSaveLines(storyFile, inputLinesArr);
  if (!closeFile(storyFile)) {
    freeCategoryWords(categoryArr);
    freeReadLines(inputLinesArr);
    exit(EXIT_FAILURE);
  }
  replaceBlanks(inputLinesArr, categoryArr, option_n);
  printReadLines(inputLinesArr);

  freeCategoryWords(categoryArr);
  freeReadLines(inputLinesArr);
  return EXIT_SUCCESS;
}
