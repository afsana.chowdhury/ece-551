#include "rand_story.h"

int main(int argc, char ** argv) {
  if (argc != 2) {
    fprintf(stderr, "Error: Incorrect number of arguments!\n");
    exit(EXIT_FAILURE);
  }
  FILE * inputFile = openFileToRead(argv[1]);

  catarray_t * categoryArr = createCategoryArray();

  readAndSaveWords(inputFile, categoryArr);
  if (!closeFile(inputFile)) {
    freeCategoryWords(categoryArr);
    exit(EXIT_FAILURE);
  }
  printWords(categoryArr);
  freeCategoryWords(categoryArr);

  return EXIT_SUCCESS;
}
