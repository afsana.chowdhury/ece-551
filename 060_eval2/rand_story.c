#include "rand_story.h"

// ************************************************************************************
// Function Name: createLinesArray
//
// This function allocated memory for lines_t struct
// ************************************************************************************
lines_t * createLinesArray() {
  lines_t * inputLinesArr = malloc(sizeof(*inputLinesArr));
  inputLinesArr->lines = NULL;
  inputLinesArr->n_lines = 0;
  return inputLinesArr;
}

// ************************************************************************************
// Function Name: readAndSaveLines
//
// This function reads each line from the story template file
// And save the lines in an array
// ************************************************************************************
void readAndSaveLines(FILE * inputFile, lines_t * inputLinesArr) {
  char * currentline = NULL;
  size_t linecap;

  while (getline(&currentline, &linecap, inputFile) >= 0) {
    inputLinesArr->lines =
        realloc(inputLinesArr->lines,
                (inputLinesArr->n_lines + 1) * sizeof(inputLinesArr->lines[0]));
    inputLinesArr->lines[inputLinesArr->n_lines] = currentline;
    currentline = NULL;
    (inputLinesArr->n_lines)++;
  }
  free(currentline);
}

// ************************************************************************************
// Function Name: printReadLines
//
// This function prints the lines from story template file.
// This function can be called both before and after replacing the blanks.
// ************************************************************************************
void printReadLines(lines_t * linesArr) {
  for (size_t i = 0; i < linesArr->n_lines; i++) {
    fprintf(stdout, "%s", linesArr->lines[i]);
  }
}

// ************************************************************************************
// Function Name: replaceBlanks
//
// This functioniterates through each line in story template
// and calls replaceBlankOneLine function to replace the blank(s)
// ************************************************************************************
void replaceBlanks(lines_t * linesArr, catarray_t * cats, int option_n) {
  // create a category to keep track of used words
  used_t * usedWords = createUsedCat();

  for (size_t i = 0; i < linesArr->n_lines; i++) {
    if (replaceBlankOneLine(&(linesArr->lines[i]), cats, usedWords, option_n) == 0) {
      freeReadLines(linesArr);
      freeCategoryWords(cats);
      freeUsedWords(usedWords);
      exit(EXIT_FAILURE);
    }
  }
  freeUsedWords(usedWords);
}

// ************************************************************************************
// Function Name: replaceBlankOneLine
//
// This function replaces the blanks with a word from the requested category
// This function returns an integer (1 for success and 0 for failure)
// to let the calling function replaceBlanks know of any error
// as it doesn't have access to all the memory that need to be free-d on exit
//
// Note that this function can detect following error cases:
// 1) Missing matching _ (uderscore) after blank
// 2) Invalid category name (passed from chooseWord_helper function)
// 3) Category with no words (passed from chooseWord_helper function)
// ************************************************************************************
int replaceBlankOneLine(char ** line,
                        catarray_t * cats,
                        used_t * usedWords,
                        int option_n) {
  char * underscore = NULL;
  char * start = *line;
  while ((underscore = strchr(start, '_')) != NULL) {  // the underscore before blank
    char * matchingUnderscore = strchr(underscore + 1, '_');
    if (matchingUnderscore == NULL) {
      fprintf(stderr,
              "Error: Formatting of blank is wrong - Couldn't find matching '_'!\n");
      return 0;
    }
    // splitting the line in substrings
    *underscore = '\0';  // break the string
    char * substrBeforeBlank = strdup(start);
    *matchingUnderscore = '\0';  // separate the word in blank
    char * templateWord = strdup(underscore + 1);
    char * substrAfterBlank = strdup(matchingUnderscore + 1);
    char * replacedWord = chooseWord_helper(templateWord, cats, usedWords, option_n);
    if (replacedWord == NULL) {  // invalid category name in template
      free(substrBeforeBlank);
      free(substrAfterBlank);
      return 0;
    }
    free(*line);  // freeing previous saved line
    *line = mergeSubStrings(substrBeforeBlank, replacedWord, substrAfterBlank);
    start = *line;
  }
  return 1;  // success
}

// ************************************************************************************
// Function Name: mergeSubStrings
//
// This function merges three strings and returns a pointer to the merged string
// ************************************************************************************
char * mergeSubStrings(char * substrBeforeBlank,
                       char * replacedWord,
                       char * substrAfterBlank) {
  size_t length =
      strlen(substrBeforeBlank) + strlen(replacedWord) + strlen(substrAfterBlank);
  char * line = malloc((length + 1) * sizeof(*line));  // +1 for '\0'
  line = strcpy(line, substrBeforeBlank);              // copying as first part
  line = strcat(line, replacedWord);
  line = strcat(line, substrAfterBlank);
  free(substrBeforeBlank);
  free(replacedWord);
  free(substrAfterBlank);
  return line;
}

// ************************************************************************************
// Function Name: chooseWord_helper
//
// This function is a helper function for the provided chooseWord function
// It checks if the blank has a valid integer or a valid category name
// Note although chooseWord can detect errors, we cannot cleanly exit
// That's why we're handling all the error cases in this helper function
// ************************************************************************************
char * chooseWord_helper(char * category,
                         catarray_t * cats,
                         used_t * usedWords,
                         int option_n) {
  const char * selectedWord = NULL;
  char * replacedWord = NULL;  // will be malloc-ed
  size_t offset = isValidInt(category, usedWords->n_words);
  if (offset) {  // valid integer
    selectedWord = findUsed(usedWords, offset);
    replacedWord = strdup(selectedWord);
    addUsedWord(replacedWord, usedWords);
    free(category);
    return replacedWord;
  }

  category_t * catInArr = NULL;
  // Error checking if the blank is not a valid integer
  if (cats != NULL) {
    catInArr = lookUpCategory(category, cats);
    if (catInArr == NULL) {
      fprintf(stderr, "Error: Invalid category name \"%s\"in template!\n", category);
      free(category);
      return NULL;
    }
    else {                           // existent category
      if (catInArr->n_words == 0) {  // category with no words
        fprintf(stderr, "Error: Category \"%s\" doesn't have any word left!\n", category);
        free(category);
        return NULL;
      }
    }
  }
  selectedWord = chooseWord(category, cats);
  replacedWord = strdup(selectedWord);
  addUsedWord(replacedWord, usedWords);
  if (cats != NULL && option_n) {
    removeWord(selectedWord, catInArr);
  }
  free(category);
  return replacedWord;
}

// ************************************************************************************
// Function Name: isValidInt
//
// This function checks if the blank has a valid integer
// A valid integer i.e. offset from current blank
// CANNOT be greater than number of blanks replaced so far
// ************************************************************************************
size_t isValidInt(char * string, size_t n_words) {
  char * endptr = NULL;
  size_t offset = strtol(string, &endptr, 10);
  if ((*endptr != '\0') || (endptr == string)) {
    return 0;
  }
  else if (offset >= 1 && offset <= n_words && n_words != 0) {
    return offset;
  }
  else {
    return 0;
  }
}

// ************************************************************************************
// Function Name: findUsed
//
// This function returns pointer to the used word based on
// valid integer (offset) provided in the blank
// ************************************************************************************
const char * findUsed(used_t * usedWords, size_t offset) {
  const char * word = usedWords->words[usedWords->n_words - offset];
  return word;
}

// ************************************************************************************
// Function Name: removeWord
//
// This function remove a word from the category array
// for no reuse of word.
// It calls moveArrUp to move the elements up of the arr by 1
// and reallocates the arr for smaller by 1 memory
// ************************************************************************************
void removeWord(const char * word, category_t * catInArr) {
  for (size_t i = 0; i < catInArr->n_words; i++) {
    if (!strcmp(word, catInArr->words[i])) {
      moveArrUp(catInArr, i);
      catInArr->words =
          realloc(catInArr->words, (catInArr->n_words - 1) * sizeof(*(catInArr->words)));
      (catInArr->n_words)--;
      break;
    }
  }
}

// ************************************************************************************
// Function Name: moveArrUp
//
// This function moves elemnents of an array by 1
// Note it leaves the last elements as-is as it's discarded anyways
// ************************************************************************************
void moveArrUp(category_t * catInArr, size_t start_index) {
  free(catInArr->words[start_index]);
  for (size_t i = start_index; i < (catInArr->n_words - 1); i++) {
    catInArr->words[i] = catInArr->words[i + 1];
  }
}

// ************************************************************************************
// Function Name: freeReadLines
//
// This function deallocates all memory in lines struct
// ************************************************************************************
void freeReadLines(lines_t * linesArr) {
  if (linesArr == NULL) {
    return;
  }
  // free individual lines
  for (size_t i = 0; i < linesArr->n_lines; i++) {
    free(linesArr->lines[i]);
  }
  // free array of lines
  free(linesArr->lines);
  // free the struct
  free(linesArr);
}

// ************************************************************************************
// Function Name: freeUsedWords
//
// This function deallocates all memory in used words array
// ************************************************************************************
void freeUsedWords(used_t * usedWords) {
  if (usedWords == NULL) {
    return;
  }

  // free individual words
  for (size_t i = 0; i < usedWords->n_words; i++) {
    free(usedWords->words[i]);
  }
  // free array of lines
  free(usedWords->words);
  // free the struct
  free(usedWords);
}

// ************************************************************************************
// Function Name: createCategoryArray()
//
// This function allocates memory for category array
// ************************************************************************************
catarray_t * createCategoryArray() {
  catarray_t * categoryArr = malloc(sizeof(*categoryArr));
  categoryArr->arr = NULL;
  categoryArr->n = 0;

  return categoryArr;
}

// ************************************************************************************
// Function Name: readAndSaveWords
//
// This function reads each line from the category/word file
// and saves them in the category array data structure
//
// Note this function can detect follownig error case:
// 1) missing colon in the line (wrong formatting)
// ************************************************************************************
void readAndSaveWords(FILE * inputFile, catarray_t * categoryArr) {
  char * currentline = NULL;
  size_t linecap;

  while (getline(&currentline, &linecap, inputFile) >= 0) {
    char * colon = strchr(currentline, ':');
    if (colon == NULL) {
      fprintf(stderr, "Error: missing colon in line:\n%s", currentline);
      free(currentline);
      freeCategoryWords(categoryArr);
      fclose(inputFile);
      exit(EXIT_FAILURE);
    }
    removeNewLineChar(currentline);
    addCategoryWord(currentline, colon, categoryArr);
    free(currentline);
    currentline = NULL;
  }
  free(currentline);
}

// ************************************************************************************
// Function Name: readAndSaveWords
//
// This function simply replaces new line character with null terminator
// ************************************************************************************
void removeNewLineChar(char * currentline) {
  char * newLine = strchr(currentline, '\n');
  if (newLine != NULL) {  // sanity check
    *newLine = '\0';
  }
}

// ************************************************************************************
// Function Name: addCategoryWord
//
// This function calls addCategory and addWord to add category and word
// from the category/word input file
// ************************************************************************************
void addCategoryWord(char * currentline, char * colon, catarray_t * categoryArr) {
  categoryArr->arr =
      realloc(categoryArr->arr, (categoryArr->n + 1) * sizeof(*(categoryArr->arr)));

  *colon = '\0';  // dividing the line
  char * cat = strdup(currentline);
  category_t * category = addCategory(cat, categoryArr);
  char * word = strdup(colon + 1);
  addWord(word, category);
}

// ************************************************************************************
// Function Name:  addCategory
//
// This function adds a new category to the category array
// It returns pointer to the category of the existing/new category
// ************************************************************************************
category_t * addCategory(char * category, catarray_t * categoryArr) {
  category_t * existingcategory = lookUpCategory(category, categoryArr);
  if (existingcategory == NULL) {  // new category
    existingcategory = addNewCategory(category, categoryArr);
  }
  else {  // category already exists
    free(category);
  }
  return existingcategory;
}

// ************************************************************************************
// Function Name:  lookUpCategory
//
// This function checks if a categry already exists
// If yes, it returns a pointer to the existing category
// If no, it returns NULL
// ************************************************************************************
category_t * lookUpCategory(char * category, catarray_t * categoryArr) {
  for (size_t i = 0; i < categoryArr->n; i++) {
    if (!strcmp(category, categoryArr->arr[i].name)) {
      return &(categoryArr->arr[i]);
    }
  }
  return NULL;
}

// ************************************************************************************
// Function Name: addNewCategory
//
// This function adds a new category to the category array
// ************************************************************************************
category_t * addNewCategory(char * category, catarray_t * categoryArr) {
  categoryArr->arr =
      realloc(categoryArr->arr, (categoryArr->n + 1) * sizeof(*(categoryArr->arr)));
  categoryArr->arr[categoryArr->n].name = category;
  categoryArr->arr[categoryArr->n].words = NULL;
  categoryArr->arr[categoryArr->n].n_words = 0;
  (categoryArr->n)++;

  return &(categoryArr->arr[categoryArr->n - 1]);
}

// ************************************************************************************
// Function Name: addWord
//
// This function adds a word to the category array
// ************************************************************************************
void addWord(char * word, category_t * category) {
  category->words =
      realloc(category->words, (category->n_words + 1) * sizeof(*(category->words)));
  category->words[category->n_words] = word;
  (category->n_words)++;
}

// ************************************************************************************
// Function Name: freeCategoryWords
//
// This function deallocates all memory in category array
// ************************************************************************************
void freeCategoryWords(catarray_t * categoryArr) {
  if (categoryArr == NULL) {
    return;
  }
  // free individual category
  for (size_t i = 0; i < categoryArr->n; i++) {
    free(categoryArr->arr[i].name);
    for (size_t j = 0; j < categoryArr->arr[i].n_words; j++) {
      free(categoryArr->arr[i].words[j]);
    }
    free(categoryArr->arr[i].words);
  }
  // free array of categories
  free(categoryArr->arr);
  // free the struct
  free(categoryArr);
}

// ************************************************************************************
// Function Name: openFileToRead
//
// This function opens a file in read mode
// ************************************************************************************
FILE * openFileToRead(char * filename) {
  FILE * inputFile = fopen(filename, "r");
  if (inputFile == NULL) {
    fprintf(stderr, "Error: Couldn't open file %s\n", filename);
    exit(EXIT_FAILURE);
  }
  return inputFile;
}

// ************************************************************************************
// Function Name: closeFile
//
// This function closes a file and returns
// 1 if it successfully closed the file
// 0 if it couldn't close the file
// ************************************************************************************
int closeFile(FILE * filename) {
  int fileClosed = fclose(filename);
  if (fileClosed == EOF) {
    fprintf(stderr, "Error: Couldn't close file!\n");
    return 0;
  }
  else {  // successfully closed file
    return 1;
  }
}

// ************************************************************************************
// Function Name: createUsedCat
//
// This function allocates memory for used words array
// ************************************************************************************
used_t * createUsedCat() {
  used_t * usedWords = malloc(sizeof(*usedWords));
  usedWords->words = NULL;
  usedWords->n_words = 0;
  return usedWords;
}

// ************************************************************************************
// Function Name: addUsedWord
//
// This function adds a word in the used word array
// as we fill up the blanks
// ************************************************************************************
void addUsedWord(const char * word, used_t * usedWords) {
  usedWords->words =
      realloc(usedWords->words, (usedWords->n_words + 1) * sizeof(*(usedWords->words)));
  usedWords->words[usedWords->n_words] = strdup(word);
  (usedWords->n_words)++;
}

// ************************************************************************************
// Function Name: checkArgOption
//
// This function checks the option in the arguement to the program
// ************************************************************************************
int checkArgOption(char * option) {
  if (strcmp(option, "-n")) {  // if given option is NOT -n
    fprintf(stderr, "Error: Wrong option! Please try \"-n\"\n");
    exit(EXIT_FAILURE);
  }
  return 1;
}
