#include "rand_story.h"

int main(int argc, char ** argv) {
  if (argc != 3) {
    fprintf(stderr, "Error: Incorrect number of arguments!\n");
    exit(EXIT_FAILURE);
  }
  FILE * catFile = openFileToRead(argv[1]);
  FILE * storyFile = fopen(argv[2], "r");
  if (storyFile == NULL) {
    fprintf(stderr, "Error: Couldn't open file %s\n", argv[2]);
    fclose(catFile);
    exit(EXIT_FAILURE);
  }
  // parse the category file first
  catarray_t * categoryArr = createCategoryArray();
  readAndSaveWords(catFile, categoryArr);
  if (!closeFile(catFile)) {
    freeCategoryWords(categoryArr);
    exit(EXIT_FAILURE);
  }

  // now parse the template and replace blanks
  lines_t * inputLinesArr = createLinesArray();
  readAndSaveLines(storyFile, inputLinesArr);
  if (!closeFile(storyFile)) {
    freeCategoryWords(categoryArr);
    freeReadLines(inputLinesArr);
    exit(EXIT_FAILURE);
  }
  replaceBlanks(inputLinesArr, categoryArr, 0);
  printReadLines(inputLinesArr);

  freeCategoryWords(categoryArr);
  freeReadLines(inputLinesArr);
  return EXIT_SUCCESS;
}
