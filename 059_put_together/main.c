#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "counts.h"
#include "kv.h"
#include "outname.h"

void removeNewLineChar(char ** string) {
  char * newLine = strchr(*string, '\n');
  *newLine = '\0';
}

counts_t * countFile(const char * filename, kvarray_t * kvPairs) {
  FILE * fileKeys = fopen(filename, "r");
  if (fileKeys == NULL) {
    fprintf(stderr, "Error opening file: %s\n", filename);
    exit(EXIT_FAILURE);
  }

  char * key = NULL;
  size_t linecap;

  counts_t * countsArray = createCounts();

  while (getline(&key, &linecap, fileKeys) >= 0) {
    removeNewLineChar(&key);
    char * val = lookupValue(kvPairs, key);
    addCount(countsArray, val);

    free(key);
    key = NULL;
  }
  free(key);
  fclose(fileKeys);

  return countsArray;
}

int main(int argc, char ** argv) {
  //WRITE ME (plus add appropriate error checking!)
  //read the key/value pairs from the file named by argv[1] (call the result kv)
  if (argc < 3) {
    fprintf(stderr, "Error: Please check number of arguments\n");
    return EXIT_FAILURE;
  }

  kvarray_t * kv = readKVs(argv[1]);
  //printKVs(kv);

  //count from 2 to argc (call the number you count i)
  for (int i = 2; i < argc; i++) {
    //count the values that appear in the file named by argv[i], using kv as the key/value pair
    //   (call this result c)
    counts_t * c = countFile(argv[i], kv);
    //compute the output file name from argv[i] (call this outName)
    char * outName = computeOutputFileName(argv[i]);

    //open the file named by outName (call that f)
    FILE * f = fopen(outName, "w");
    if (f == NULL) {
      fprintf(stderr, "Error opening output file\n");
      return EXIT_FAILURE;
    }

    //print the counts from c into the FILE f
    printCounts(c, f);

    //close f
    fclose(f);

    //free the memory for outName and c
    free(outName);

    freeCounts(c);
  }
  //free the memory for kv
  freeKVs(kv);

  return EXIT_SUCCESS;
}
