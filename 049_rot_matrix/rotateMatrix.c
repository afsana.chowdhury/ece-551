#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LINE_SIZE (NUMBER_OF_COLUMNS + 2)
#define NUMBER_OF_ROWS \
  10  // number of rows of given matrix. It'll be the number of columns of the 90 degrees rotated matrix
#define NUMBER_OF_COLUMNS \
  10  // number of columns of given matrix. It'll be the number of  rows of the 90 degrees rotated matrix
void readFileAndSaveArray(char matrix[NUMBER_OF_COLUMNS][NUMBER_OF_ROWS],
                          FILE * inputFile) {
  char line[LINE_SIZE];  // 10 + new line
  int row = 0;

  while (fgets(line, LINE_SIZE, inputFile) != NULL) {
    char * newLineChar = strchr(line, '\n');

    if (newLineChar != &line[10])  // No new line at index 10
    {
      fprintf(stderr,
              "Rows are of wrong size! Remember it should be a 10 X 10 matrix.\n");
      exit(EXIT_FAILURE);
    }
    // store the line characters in matrix
    for (int i = 0; i < NUMBER_OF_COLUMNS; i++) {
      matrix[i][NUMBER_OF_ROWS - 1 - row] = line[i];
    }

    row++;
  }

  if (row != 10)  // Index 0-9 for 10 lines
  {
    fprintf(stderr,
            "Number of lines is wrong! Remember it should be a 10 X 10 matrix.\n");
    exit(EXIT_FAILURE);
  }
}

void printArray(char matrix[NUMBER_OF_COLUMNS][NUMBER_OF_ROWS]) {
  for (int row = 0; row < NUMBER_OF_COLUMNS; row++) {
    for (int column = 0; column < NUMBER_OF_ROWS; column++) {
      fprintf(stdout, "%c", matrix[row][column]);
    }
    fprintf(stdout, "\n");
  }
}

int main(int argc, char ** argv) {
  // Check correct number of argumnents
  if (argc != 2)  // should take only one filename as argc
  {
    fprintf(stderr,
            "Wrong number of arguments! Please run again with correct arguments: "
            "./rotateMatrix inputFileName\n");
    return EXIT_FAILURE;
  }

  // Try opening the file
  FILE * f = fopen(argv[1], "r");
  if (f == NULL) {
    fprintf(stderr,
            "Couldn't open file. Please check the file name and if file exists.\n");
    return EXIT_FAILURE;
  }

  char matrix[NUMBER_OF_ROWS][NUMBER_OF_COLUMNS];

  readFileAndSaveArray(matrix, f);
  printArray(matrix);

  return EXIT_SUCCESS;
}
