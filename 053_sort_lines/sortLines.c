#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void readandSortLines(FILE * readFrom);

//This function is used to figure out the ordering of the strings
//in qsort. You do not need to modify it.
int stringOrder(const void * vp1, const void * vp2) {
  const char * const * p1 = vp1;
  const char * const * p2 = vp2;
  return strcmp(*p1, *p2);
}
//This function will sort data (whose length is count).
void sortData(char ** data, size_t count) {
  qsort(data, count, sizeof(char *), stringOrder);
}

int main(int argc, char ** argv) {
  FILE * readFrom;

  if (argc == 1) {  // no arg other than program name
    readandSortLines(stdin);
  }
  else {  //More than 1 argument. Treat each arg as an input file
    int numOfInputFiles = argc - 1;
    int fileToRead = 1;

    do {
      readFrom = fopen(argv[fileToRead], "r");
      if (readFrom == NULL) {
        fprintf(stderr, "Error opening file %d\n", fileToRead);
        exit(EXIT_FAILURE);
      }
      readandSortLines(readFrom);
      fclose(readFrom);
      fileToRead++;
    } while (fileToRead <= numOfInputFiles);
  }

  return EXIT_SUCCESS;
}

void readandSortLines(FILE * readFrom) {
  size_t sz_malloc = 0;   // size of malloced buffer
  size_t numOfLines = 0;  //the number of chars written (-1 for error)
  char ** lines = NULL;   // to hold the array of strings
  char * curr = NULL;     // to hold each line read

  while (getline(&curr, &sz_malloc, readFrom) >= 0) {
    lines = realloc(lines, (numOfLines + 1) * sizeof(*lines));
    lines[numOfLines] = curr;
    curr = NULL;
    numOfLines++;
  }
  free(curr);
  sortData(lines, numOfLines);
  for (size_t readLine = 0; readLine < numOfLines; readLine++) {
    printf("%s", lines[readLine]);
    free(lines[readLine]);
  }
  free(lines);
}
