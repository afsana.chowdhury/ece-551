#include <stdio.h>
#include <stdlib.h>

void updateLengths(size_t * current_length, size_t * largest_length) {
  if (*current_length > *largest_length) {
    *largest_length = *current_length;
  }

  *current_length = 1;
}

size_t maxSeq(int * array, size_t n) {
  size_t largest_length, current_length;

  // Empty array
  if (n == 0) {
    return 0;
  }

  // otherwise
  current_length = 1;
  largest_length = 1;
  for (size_t i = 1; i < n; i++) {
    if (array[i] > array[i - 1]) {
      current_length += 1;
    }

    else {
      updateLengths(&current_length, &largest_length);
    }
  }

  updateLengths(&current_length, &largest_length);
  return largest_length;
}
