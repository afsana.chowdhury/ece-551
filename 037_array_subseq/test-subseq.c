#include <stdio.h>
#include <stdlib.h>

// function prototype
size_t maxSeq(int * array, size_t n);

//Helper Function
// Calling maxSeq function and checking the answer
void runCheck(int * array, size_t n, size_t expected_result) {
  size_t received_result = maxSeq(array, n);
  printf("expected %ld\n", expected_result);

  if (expected_result != received_result) {
    printf("Failed with array starting with %d and size %ld, received %ld\n",
           *array,
           n,
           received_result);
    exit(EXIT_FAILURE);
  }
}

int main(void) {
  int * test_array1 = NULL;
  int test_array2[2] = {0, 0};
  int test_array3[1] = {5};
  int test_array4[500] = {0};
  int test_array5[5] = {1, 2, 3, 4, 5};
  int test_array6[5] = {5, 4, 3, 2, 1};
  int test_array7[] = {1, 2, 1, 3, 5, 7, 2, 4, 6, 9};
  int test_array8[] = {1, 1000};
  int test_array9[] = {-3, -2, 0, 1};

  runCheck(test_array1, 0, 0);
  runCheck(test_array2, 2, 1);
  runCheck(test_array3, 1, 1);
  runCheck(test_array4, 500, 1);
  runCheck(test_array5, 5, 5);
  runCheck(test_array6, 5, 1);
  runCheck(test_array7, 10, 4);
  runCheck(test_array8, 2, 2);
  runCheck(test_array9, 4, 4);

  return EXIT_SUCCESS;
}
