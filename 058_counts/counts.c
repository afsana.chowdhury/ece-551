#include "counts.h"

int nameExists(counts_t * c, const char * name) {
  for (size_t i = 0; i < c->countNames; i++) {
    if (!strcmp(c->namesArray[i].name, name)) {  // if names are same
      c->namesArray[i].count++;
      return 1;
    }
  }
  return 0;
}

counts_t * createCounts(void) {
  counts_t * countArray = malloc(sizeof(*countArray));
  countArray->namesArray = NULL;
  countArray->countNames = 0;
  countArray->countUnknown = 0;

  return countArray;
}
void addCount(counts_t * c, const char * name) {
  if (name == NULL) {
    (c->countUnknown)++;
    return;
  }

  if (c->countNames > 0) {
    if (nameExists(c, name)) {
      return;
    }
  }

  // if we've reached here, that means it's a new name

  c->namesArray = realloc(c->namesArray, (c->countNames + 1) * sizeof(*c->namesArray));
  c->namesArray[c->countNames].name =
      malloc((strlen(name) + 1) * sizeof(*(c->namesArray[c->countNames].name)));

  strcpy(c->namesArray[c->countNames].name, name);
  c->namesArray[c->countNames].count = 1;
  c->countNames++;
}
void printCounts(counts_t * c, FILE * outFile) {
  for (size_t i = 0; i < c->countNames; i++) {
    fprintf(outFile, "%s: %ld\n", c->namesArray[i].name, c->namesArray[i].count);
  }
  if (c->countUnknown > 0) {
    fprintf(outFile, "<unknown> : %ld\n", c->countUnknown);
  }
}

void freeCounts(counts_t * c) {
  for (size_t i = 0; i < c->countNames; i++) {
    free(c->namesArray[i].name);  // first free the name string
  }
  free(c->namesArray);  // now free the struct for that name
  free(c);
}
