#include "vector.hpp"

#include <cmath>
#include <cstdio>

// Class Vector2D Implementation

Vector2D::Vector2D() : x(0), y(0) {
}
Vector2D::Vector2D(double x, double y) : x(x), y(y) {
}

double Vector2D::getMagnitude() const {
  double magnitude = std::sqrt(x * x + y * y);
  return magnitude;
}

Vector2D Vector2D::operator+(const Vector2D & rhs) const {
  Vector2D sum;
  sum.x = x + rhs.x;
  sum.y = y + rhs.y;

  return sum;
}

Vector2D & Vector2D::operator+=(const Vector2D & rhs) {
  x = x + rhs.x;
  y = y + rhs.y;
  return *this;
}

double Vector2D::dot(const Vector2D & rhs) const {
  double dot = x * rhs.x + y * rhs.y;
  return dot;
}

void Vector2D::print() const {
  std::printf("<%.2f, %.2f>", x, y);
}
