#include "kv.h"

void readLines(FILE * file1, lines_t * thisLine) {
  char * curr = NULL;
  size_t linecap;
  size_t num_of_lines = 0;

  char ** lines = NULL;

  while (getline(&curr, &linecap, file1) >= 0) {
    lines = realloc(lines, (num_of_lines + 1) * sizeof(*lines));
    lines[num_of_lines] = curr;
    curr = NULL;
    num_of_lines++;
  }

  free(curr);
  thisLine->lines = lines;
  thisLine->num_of_lines = num_of_lines;
}

void splitOneLine(char * oneLine, kvpair_t * kvpair_thisLine) {
  if (strlen(oneLine) == 0) {
    fprintf(stderr, "Error: Empty line\n");
    exit(EXIT_FAILURE);
  }

  char * readLine = oneLine;

  // read key first
  int keycharacters = 0;
  while (*readLine != '=') {
    kvpair_thisLine->key = realloc(kvpair_thisLine->key,
                                   (keycharacters + 1) * sizeof(*kvpair_thisLine->key));
    kvpair_thisLine->key[keycharacters++] = *readLine;
    readLine++;  // point to next character
  }

  kvpair_thisLine->key =
      realloc(kvpair_thisLine->key, (keycharacters + 1) * sizeof(*kvpair_thisLine->key));
  kvpair_thisLine->key[keycharacters] = '\0';

  // read value now
  readLine++;  // pointing to character after first = sign
  kvpair_thisLine->value =
      malloc((strlen(readLine) + 1) *
             (sizeof(*(kvpair_thisLine->value))));  // allocate memory for value
  char * valueEnd = stpcpy(kvpair_thisLine->value, readLine);
  if (valueEnd == kvpair_thisLine->value) {  // no characters copied
    fprintf(stderr, "Error: Missing value\n");
    exit(EXIT_FAILURE);
  }
  else if (*(valueEnd - 1) == '\n') {
    *(valueEnd - 1) = '\0';
  }
}

kvarray_t * splitLines(lines_t * linesArray, kvarray_t * kv_pairs_array) {
  // for every line,  split it

  for (size_t i = 0; i < linesArray->num_of_lines; i++) {
    kvpair_t * kvpair_oneLine =
        malloc(sizeof(*kvpair_oneLine));  // struct holding kv pair of a specific line
    kvpair_oneLine->key = NULL;
    kvpair_oneLine->value = NULL;

    splitOneLine(linesArray->lines[i], kvpair_oneLine);
    free(linesArray->lines[i]);

    // now add this struct to the array
    kv_pairs_array->kv_pairs =
        realloc(kv_pairs_array->kv_pairs, (i + 1) * sizeof(*kv_pairs_array->kv_pairs));
    kv_pairs_array->kv_pairs[i].key = kvpair_oneLine->key;
    kv_pairs_array->kv_pairs[i].value = kvpair_oneLine->value;
    kv_pairs_array->numOfPairs += 1;

    // we're done reading one line, so free it
    free(kvpair_oneLine);
  }
  free(linesArray->lines);

  return kv_pairs_array;
}

kvarray_t * readKVs(const char * fname) {
  // open the file
  FILE * file1 = fopen(fname, "r");
  if (file1 == NULL) {
    fprintf(stderr, "Error while opening file\n");

    exit(EXIT_FAILURE);
  }

  // read lines from file
  lines_t * linesArray = malloc(sizeof(*linesArray));
  linesArray->lines = NULL;
  linesArray->num_of_lines = 0;

  readLines(file1, linesArray);

  // split lines into key/value pairs
  kvarray_t * kv_pairs_array = malloc(sizeof(*kv_pairs_array));
  kv_pairs_array->kv_pairs = NULL;
  kv_pairs_array->numOfPairs = 0;
  kv_pairs_array = splitLines(linesArray, kv_pairs_array);

  fclose(file1);
  free(linesArray);

  return kv_pairs_array;
}

void freeKVs(kvarray_t * pairs) {
  //free the structs first
  for (size_t i = 0; i < pairs->numOfPairs; i++) {
    free(pairs->kv_pairs[i].key);
    free(pairs->kv_pairs[i].value);
  }
  free(pairs->kv_pairs);
  free(pairs);
}

void printKVs(kvarray_t * pairs) {
  for (size_t i = 0; i < pairs->numOfPairs; i++) {
    printf("key = '%s' value = '%s'\n", pairs->kv_pairs[i].key, pairs->kv_pairs[i].value);
  }
}

char * lookupValue(kvarray_t * pairs, const char * key) {
  for (size_t i = 0; i < pairs->numOfPairs; i++) {
    if (strcmp(key, pairs->kv_pairs[i].key) == 0) {  // if same
      return pairs->kv_pairs[i].value;
    }
  }
  return NULL;
}
