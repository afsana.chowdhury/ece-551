#ifndef __LL_HPP__
#define __LL_HPP__

#include <assert.h>

#include <cstdlib>
#include <exception>
#include <string>

class Tester;

class invalidItem : public std::exception {
  const char * err_msg;

 public:
  invalidItem(const char * msg) : err_msg(msg){};
  invalidItem(const std::string msg) : err_msg(msg.c_str()){};
  virtual const char * what() const throw() { return err_msg; }
};

template<typename T>
class LinkedList {
 private:
  class Node {
   public:
    T data;
    Node * next;
    Node * prev;
    Node() : next(NULL), prev(NULL){};
    Node(T d, Node * n, Node * p) : data(d), next(n), prev(p){};
  };
  Node * head;
  Node * tail;
  size_t size;

 public:
  LinkedList() : head(NULL), tail(NULL), size(0) {}
  LinkedList(const LinkedList & rhs) {
    head = NULL;
    tail = NULL;
    size = 0;
    Node * curr_rhs = rhs.head;
    while (curr_rhs != NULL) {
      addBack(curr_rhs->data);
      curr_rhs = curr_rhs->next;
    }
  }
  LinkedList & operator=(const LinkedList & rhs) {
    if (this != &rhs) {
      LinkedList * tempLL = new LinkedList(rhs);
      Node * old_head = head;
      Node * old_tail = tail;
      size_t old_size = size;

      // pointing to new nodes
      head = tempLL->head;
      tail = tempLL->tail;
      size = tempLL->size;

      // pointing to old nodes
      tempLL->head = old_head;
      tempLL->tail = old_tail;
      tempLL->size = old_size;

      delete tempLL;
    }
    return *this;
  }
  ~LinkedList() {
    while (head != NULL) {
      Node * temp = head->next;
      delete head;
      head = temp;
    }
  }
  void addFront(const T & item) {
    head = new Node(item, head, NULL);
    if (tail == NULL) {  // list was empty before
      tail = head;
    }
    else {
      head->next->prev = head;
    }
    size++;
  }
  void addBack(const T & item) {
    tail = new Node(item, NULL, tail);
    if (head == NULL) {  // list was empty before
      head = tail;
    }
    else {
      tail->prev->next = tail;
    }
    size++;
  }
  bool remove(const T & item) {
    Node ** curr = &head;
    while (*curr != NULL) {
      Node * temp = *curr;
      if (temp->data == item) {
        if (temp->prev != NULL) {        // not the first item
          *curr = temp->next;            // changed next of node before
          if (temp->next != NULL) {      // not the last item
            (*curr)->prev = temp->prev;  // changing prev of node after
          }
          else {  // last item
            tail = temp->prev;
          }
        }
        else {                        // the first element
          if (temp->next != NULL) {   // not the last node
            temp->next->prev = NULL;  // prev of next node changed
            head = temp->next;
          }
          else {  // the last node
            head = NULL;
            tail = NULL;
          }
        }
        size--;
        delete temp;
        return true;
      }
      else {
        curr = &(temp->next);
      }
    }
    return false;
  }
  T & operator[](int index) {
    if (index < 0 || index >= (int)size) {
      throw invalidItem("Invalid Index!\n");
    }
    Node * current = head;
    while (index > 0) {
      current = current->next;
      index--;
    }
    return current->data;
  }
  const T & operator[](int index) const {
    if (index < 0 || index >= (int)size) {
      throw invalidItem("Invalid Index!\n");
    }
    Node * current = head;
    while (index > 0) {
      current = current->next;
      index--;
    }
    return current->data;
  }
  int find(const T & item) {
    Node * curr = head;
    int index = 0;
    while (curr != NULL) {
      if (curr->data == item) {
        return index;
      }
      else {
        curr = curr->next;
      }
      index++;
    }

    index = -1;  // no such item
    return index;
  }
  int getSize() const { return size; }
  friend class Tester;
};

#endif
