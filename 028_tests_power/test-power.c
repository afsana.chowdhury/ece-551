#include <stdio.h>
#include <stdlib.h>

// Function prototype
unsigned power(unsigned x, unsigned y);

// Helper Function
// Calls power and checks the result is expected_ans
void run_check(unsigned x, unsigned y, unsigned expected_ans) {
  unsigned returned_ans = power(x, y);

  if (returned_ans != expected_ans) {
    printf("Failed: expected %d, received %d", expected_ans, returned_ans);
    exit(EXIT_FAILURE);
  }
}

// Main Funtion
// Testing function power
int main() {
  // Test cases
  run_check(2, 3, 8);
  run_check(0, 3, 0);
  run_check(4, 0, 1);
  run_check(200, 2, 40000);
  run_check(2, 15, 32768);
  run_check(0, 0, 1);
  run_check(1, 2, 1);
  run_check(1, 3, 1);
  run_check(1000, 2, 1000000);
  run_check(2, 30, 1073741824);
  run_check(1000000, 2, (unsigned)1000000000000);

  return EXIT_SUCCESS;
}
