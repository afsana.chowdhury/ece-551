#include "circle.hpp"

#include <math.h>
#include <stdio.h>

Circle::Circle(Point inCenter, double inRadius) : center(inCenter), radius(inRadius) {
}

void Circle::move(double dx, double dy) {
  center.move(dx, dy);
}
double Circle::intersectionArea(const Circle & otherCircle) {
  double area = 0;
  if (radius == 0 || otherCircle.radius == 0) {
    return 0;
  }
  double d = center.distanceFrom(otherCircle.center);
  double area1 = 3.1415926535 * pow(radius, 2);
  double area2 = 3.1415926535 * pow(otherCircle.radius, 2);
  if (d == 0) {  // same center

    if (area1 < area2)
      return area1;
    else
      return area2;
  }
  if (d >= (radius + otherCircle.radius)) {
    // circles don't intersect
    return 0;
  }
  if (radius - otherCircle.radius >= d) {
    return area2;
  }
  else if (otherCircle.radius - radius >= d) {
    return area1;
  }

  double d1 = (pow(radius, 2) - pow(otherCircle.radius, 2) + pow(d, 2)) / (2 * d);
  //printf("d1: %f\n", d1);

  double d2 = d - d1;
  //printf("d2: %f\n", d2);
  double A1 = pow(radius, 2) * acos(d1 / radius) - d1 * sqrt(pow(radius, 2) - pow(d1, 2));
  //printf("A1: %f\n", A1);

  double A2 = pow(otherCircle.radius, 2) * acos(d2 / otherCircle.radius) -
              d2 * sqrt(pow(otherCircle.radius, 2) - pow(d2, 2));
  //printf("A2: %f\n", A2);
  /*
  double A1 =
      pow(radius, 2) *
      acos((pow(d, 2) + pow(radius, 2) - pow(otherCircle.radius, 2)) / (2 * d * radius));
  double A2 = pow(otherCircle.radius, 2) *
              acos((pow(d, 2) + pow(otherCircle.radius, 2) - pow(radius, 2)) /
                   (2 * d * otherCircle.radius));

  double A3 =
      sqrt((-d + radius + otherCircle.radius) * (d + radius - otherCircle.radius) *
           (d - radius + otherCircle.radius) * (d + radius + otherCircle.radius)) /
      2;
  */
  area = A1 + A2;
  return area;
}
