#ifndef CIRCLE
#define CIRCLE
#include "point.hpp"

class Circle {
  Point center;
  const double radius;

 public:
  Circle(Point inCenter, double inRadius);
  void move(double dx, double dy);
  double intersectionArea(const Circle & otherCircle);
};
#endif
