//********************************************************************************************
//*****************************ECE 551 Eval-1************************************************
// Student Name: Afsana Chowdhury
// NetID: ahc35
//
// Functions in this file:
//      Given by instructor:
//                  country_t parseLine(char * line)
//                  void calcRunningAvg(unsigned * data, size_t n_days, double * avg)
//                  void calcCumulative(unsigned * data, size_t n_days, uint64_t pop, double * cum)
//                  void printCountryWithMax(country_t * countries, size_t n_countries, unsigned ** data, size_t n_days)

//
//      Added for abstraction:
//                  void checkNameFormatting(char * line);
//                  char * readCountryName(char * line, country_t * ans);
//                  void readPopulation(char * readLine, country_t * ans);
//                  unsigned findHighestOfCountry(unsigned * dailyCases, size_t n_days);
//                  void findCountryWithHighest(unsigned * highestOfCountries, country_t * countries, size_t n_countries);
//
//
//********************************************************************************************

#include "pandemic.h"

#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <string.h>

// Macros
#define RUNNING_AVG_DAYS 7
#define NUM_OF_PEOPLE_FOR_CUM 100000

// Function Prototypes
void checkNameFormatting(char * line);
char * readCountryName(char * line, country_t * ans);
void readPopulation(char * readLine, country_t * ans);

unsigned findHighestOfCountry(unsigned * dailyCases, size_t n_days);
void findCountryWithHighest(unsigned * highestOfCountries,
                            country_t * countries,
                            size_t n_countries);

// Functions Definitions

country_t parseLine(char * line) {
  country_t ans;

  if (strlen(line) == 0) {  // empty line
    fprintf(stderr, "Error in reading line: Line is empty!\n");
    exit(EXIT_FAILURE);
  }

  if (line == NULL) {  // line doesn't have correct pointer
    fprintf(stderr, "Error in reading line: NULL\n");
    exit(EXIT_FAILURE);
  }

  checkNameFormatting(line);

  // if we're here, that means for matting is correct (there is a comma + length of country name is valid)

  // Read the country name first
  char * readLine = readCountryName(line, &ans);

  readLine++;  // pointing at the first character after comma

  // Read the population now
  readPopulation(readLine, &ans);

  return ans;
}

void calcRunningAvg(unsigned * data, size_t n_days, double * avg) {
  if (data == NULL) {  // safety check in case data isn't pointing correctly
    fprintf(stderr, "Data array passed for running avg calculation isn't correct.\n");
    exit(EXIT_FAILURE);
  }

  if (avg == NULL) {  // safety check in case avg isn't pointing correctly
    fprintf(stderr, "Avg array passed for running avg calculation isn't correct.\n");
    exit(EXIT_FAILURE);
  }

  double sum = 0;
  size_t start_date = RUNNING_AVG_DAYS - 1;

  while (
      start_date <
      n_days) {  // checking if there are 7 days of data available and if we've reached the last data
    for (int i = RUNNING_AVG_DAYS - 1; i >= 0; i--) {
      sum += data[start_date - i];
    }
    *avg =
        sum /
        RUNNING_AVG_DAYS;  // no need of casting to double as "sum" is declared as double already
    avg++;
    sum = 0;  // resetting sum for next avg calculation
    start_date++;
  }
}

void calcCumulative(unsigned * data, size_t n_days, uint64_t pop, double * cum) {
  if (data == NULL) {  // safety check in case data isn't pointing correctly
    fprintf(stderr, "Data array passed for cumulative calculation isn't correct.\n");
    exit(EXIT_FAILURE);
  }

  if (cum == NULL) {  // safety check in case cum isn't pointing correctly
    fprintf(stderr, "cum array passed for cumulative calculation isn't correct.\n");
    exit(EXIT_FAILURE);
  }

  double cumulativeSoFar = 0;

  for (size_t i = 0; i < n_days; i++) {
    cumulativeSoFar += data[i];

    *cum = cumulativeSoFar / pop * NUM_OF_PEOPLE_FOR_CUM;
    // no need of casting as cumulativesofar is declared as double already
    cum++;
  }
}

void printCountryWithMax(country_t * countries,
                         size_t n_countries,
                         unsigned ** data,
                         size_t n_days) {
  if ((n_countries == 0) | (n_days == 0)) {
    return;
  }

  if (data == NULL) {  // safety check
    fprintf(stderr, "Error in data: Data is not ponting correctly.\n");
    exit(EXIT_FAILURE);
  }

  if (countries == NULL) {  // safety check
    fprintf(stderr, "Error in data: Countries is not ponting correctly.\n");
    exit(EXIT_FAILURE);
  }

  // first we'll find the highest case of each country
  // and store them in a new array
  unsigned highestOfCountries[n_countries];  // array to hold highest case of each country

  for (unsigned country = 0; country < n_countries; country++) {
    highestOfCountries[country] = findHighestOfCountry(data[country], n_days);
  }

  // Now we'll compare the highest case number of all the countries
  findCountryWithHighest(highestOfCountries, countries, n_countries);
}

// ******************New Functions Added For Abstraction*****************************

void checkNameFormatting(char * line) {
  char * comma = strchr(line, ',');  // look for comma in the line

  if (comma == NULL) {  // if there is no comma
    fprintf(stderr, "Error in format: No comma in the line!\n");
    exit(EXIT_FAILURE);
  }

  else if ((comma - line) >= (MAX_NAME_LEN - 1)) {
    // if # of chars before the comma is more than 62. Note we need to save one index for null terminator
    fprintf(stderr,
            "Error in format: Country name is too long! The allowed length is 64.\n");
    exit(EXIT_FAILURE);
  }
}

char * readCountryName(char * line, country_t * ans) {
  // Read the characters before comma and store them
  char * readLine = line;  // point at the first character of line
  int countryNameChar = 0;

  while (*readLine != ',') {  // keep reading until comma
    ans->name[countryNameChar++] = *readLine;
    readLine++;  // point to next character
  }

  // readLine is pointing at comma at this point
  // countryNamechar is index after the last read char of the country name
  // so we need to add a null terminator to make it a complete string
  ans->name[countryNameChar] = '\0';
  return readLine;
}

void readPopulation(char * readLine, country_t * ans) {
  errno = 0;  // to distinguish success/failure after strtol call

  char * endptr;  // to store the result from strtol pointing at the first non-digit char
  long population = strtol(readLine, &endptr, 10);

  /* Check for various possible errors */
  if ((errno == ERANGE && (population == LONG_MAX || population == LONG_MIN)) ||
      (errno != 0 && population == 0)) {
    fprintf(stderr, "Error in population: Overflow/Underflow detected\n");
    exit(EXIT_FAILURE);
  }

  if (endptr == readLine) {  // end pointer is at the beginning of the string
    fprintf(stderr, "Error in population: No digits were found\n");
    exit(EXIT_FAILURE);
  }
  /* If we got here, strtol() successfully parsed a number */
  ans->population = (uint64_t)population;  // casting to convert negative number
}

unsigned findHighestOfCountry(unsigned * dailyCases, size_t n_days) {
  unsigned highest = 0;
  for (unsigned i = 1; i < n_days; i++) {
    if (dailyCases[i] > dailyCases[highest]) {
      highest = i;  // updates index
    }
  }
  return dailyCases[highest];
}

void findCountryWithHighest(unsigned * highestOfCountries,
                            country_t * countries,
                            size_t n_countries) {
  unsigned highest = 0;
  int tie = 0;  // to keep track of tie

  for (unsigned country = 1; country < n_countries; country++) {
    if (highestOfCountries[country] > highestOfCountries[highest]) {
      highest = country;  // update index
      tie = 0;            // reset tie flag
    }

    else if (highestOfCountries[country] == highestOfCountries[highest]) {
      tie = 1;  // tie as they're equal
    }
  }

  // we're done comparing highest daily cases of all the countries

  if (tie) {
    printf("There is a tie between at least two countries\n");
  }
  else {
    printf("%s has the most daily cases with %u\n",
           countries[highest].name,
           highestOfCountries[highest]);
  }
}
