#include "node.h"

Node * buildTree(uint64_t * counts) {
  // Make a priority queue.
  priority_queue_t pqHuffman;

  /*For each character that appears in the input (its frequency is not 0---you have
    the frequencies passed in as an argument), you need to construct a new Node
    and add it to the priority queue. 
    Note that if you are on the ith charachter, you can do 
    pq.push(new Node(i,freqs[i])); */

  for (size_t i = 0; i < 257; i++) {
    if (counts[i] != 0) {
      pqHuffman.push(new Node(i, counts[i]));
    }
  }

  /*Next, you need to build the tree from the Nodes by removing the top two nodes,
    building a new Node out of them, and putting the new Node into the Priority Queue.*/
  while (pqHuffman.size() != 1) {
    Node * left = pqHuffman.top();
    pqHuffman.pop();
    Node * right = pqHuffman.top();
    pqHuffman.pop();

    pqHuffman.push(new Node(left, right));
  }
  // Once the priority queue has only one element, that element should be the entire tree
  // You should return the root Node of the resulting encoding tree.
  return pqHuffman.top();
}
