#include <iostream>

#include "function.h"

int binarySearchForZero(Function<int, int> * f, int low, int high) {
  int low_range = low;
  int high_range = high;

  int largestX = low;

  while (low_range < high_range) {
    int mid = (low_range + high_range - 1) / 2;
    if (f->invoke(mid) <= 0) {  // move to the second half
      largestX = mid;
      low_range = mid + 1;
    }
    else {  // move to first half
      high_range = mid;
    }
  }
  return largestX;
}
