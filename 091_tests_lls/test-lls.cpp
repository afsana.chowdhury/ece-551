#include <assert.h>

#include <cstdlib>
#include <iostream>

#include "il.hpp"

class Tester {
 public:
  // testing for default constructor is done for you
  void testDefCtor() {
    IntList il;
    assert(il.head == NULL);
    assert(il.tail == NULL);
    assert(il.getSize() == 0);
  }
  // example of another method you might want to write
  void testAddFront() {
    IntList il;  // empty list
    il.addFront(10);
    assert(il.head->data == 10);
    assert(il.head->prev == NULL);
    assert(il.head->next == NULL);
    assert(il.tail == il.head);
    assert(il.getSize() == 1);
    il.addFront(20);              // adding another node
    assert(il.head->data == 20);  // checking first/head node
    assert(il.head->prev == NULL);
    assert(il.head->next != NULL);
    assert(il.head->next->data == 10);        // checking second/last node
    assert(il.head->next->prev->data == 20);  // checking "prev" of last node
    assert(il.head->next->next == NULL);      // checking last node
    assert(il.tail->data == 10);
  }

  void testAddBack() {
    IntList il;  // empty list
    il.addBack(5);
    assert(il.head->data == 5);
    assert(il.head->prev == NULL);
    assert(il.head->next == NULL);
    assert(il.tail == il.head);
    assert(il.getSize() == 1);
    il.addBack(15);                          // adding another node
    assert(il.head->data == 5);              // checking first/head node
    assert(il.head->next->data == 15);       // checking second/last node
    assert(il.head->next->prev->data == 5);  // checking "prev" of last node
    assert(il.head->next->next == NULL);     // checking last node
    assert(il.head->prev == NULL);
    assert(il.tail->data == 15);
  }
  void testRulOfThree() {
    IntList il1;
    il1.addBack(100);
    il1.addBack(200);
    il1.addBack(300);

    // copy constructor
    testCopyConstructor(il1);
    IntList * il2 = new IntList(il1);
    assert(il2->head->data == 100);
    assert(il2->tail->data == 300);
    assert(il2->getSize() == 3);

    // assignment op
    il1.addBack(400);
    il1.addFront(0);
    *il2 = il1;
    assert(il2->head->data == 0);
    assert(il2->tail->data == 400);
    assert(il2->getSize() == 5);

    // destructor
    delete il2;
  }
  void testCopyConstructor(const IntList & LL) {
    assert(LL.head->data == 100);
    assert(LL.tail->data == 300);
    assert(LL.getSize() == 3);
  }

  void testRemove() {
    IntList il1;

    assert(il1.remove(0) == false);  // removing from empty list

    il1.addBack(100);
    assert(il1.remove(100) == true);
    assert(il1.head == NULL);
    assert(il1.tail == NULL);
    assert(il1.getSize() == 0);

    il1.addBack(100);
    il1.addBack(200);
    il1.addBack(300);
    il1.addBack(400);
    il1.addBack(500);

    assert(il1.remove(1000) == false);  // invalid item
    assert(il1.remove(100) == true);    // removed head
    assert(il1.head->data == 200);
    assert(il1.head->prev == NULL);
    assert(il1.head->next->data == 300);
    assert(il1.getSize() == 4);

    assert(il1.remove(300) == true);  // remove from middle
    assert(il1.head->next->data == 400);
    assert(il1.tail->prev->prev->data == 200);

    assert(il1.remove(500));  // remove tail
    assert(il1.tail->data == 400);
    assert(il1.tail->next == NULL);
    assert(il1.tail->prev->next->data == 400);
  }
};

int main(void) {
  Tester t;
  t.testDefCtor();
  t.testAddFront();
  t.testAddBack();
  t.testRulOfThree();
  t.testRemove();
  // write calls to your other test methods here
  return EXIT_SUCCESS;
}
