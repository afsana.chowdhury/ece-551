#include "node.h"
void Node::buildMap(BitString b, std::map<unsigned, BitString> & theMap) {
  if (sym != NO_SYM) {  // leaf node
    assert(left == NULL && right == NULL);
    theMap[sym] = b;
  }
  else {
    assert(left != NULL && right != NULL);

    // recurse to left
    BitString bLeft = b.plusZero();
    left->buildMap(bLeft, theMap);

    // recurse to right
    BitString bRight = b.plusOne();
    right->buildMap(bRight, theMap);
  }
}
