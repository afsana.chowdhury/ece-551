#include <ctype.h>
#include <stdio.h>
#include <string.h>

#include <cstdlib>
#include <iostream>
#include <sstream>
class Expression {
 public:
  virtual std::string toString() const = 0;  //abstract method
  virtual ~Expression() {}
};

class NumExpression : public Expression {
  long number;

 public:
  explicit NumExpression(long inputNumber) : number(inputNumber) {}
  virtual std::string toString() const {
    std::stringstream ss;
    ss << number;
    return ss.str();
  }

  ~NumExpression() {}
};

class PlusExpression : public Expression {
  Expression & lhs;
  Expression & rhs;

 public:
  explicit PlusExpression(Expression * lhs, Expression * rhs) : lhs(*lhs), rhs(*rhs) {}
  virtual std::string toString() const {
    std::stringstream ss;
    ss << "(" << lhs.toString() << " + " << rhs.toString() << ")";
    return ss.str();
  }
  ~PlusExpression() {
    delete &lhs;
    delete &rhs;
  }
};
