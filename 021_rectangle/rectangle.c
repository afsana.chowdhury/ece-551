#include <stdio.h>
#include <stdlib.h>

struct rectangle_struct {
  int x;
  int y;
  int width;
  int height;
};

typedef struct rectangle_struct rectangle;

//I've provided "min" and "max" functions in
//case they are useful to you
int min(int a, int b) {
  if (a < b) {
    return a;
  }
  return b;
}
int max(int a, int b) {
  if (a > b) {
    return a;
  }
  return b;
}

//Declare your rectangle structure here!

rectangle canonicalize(rectangle r) {
  if (r.width < 0)  // width is negative
  {
    r.width = -r.width;   //makes it positive
    r.x = r.x - r.width;  // moves the x coordinate to left one
  }
  if (r.height < 0)  //height is negative
  {
    r.height = -r.height;  // makes it positive
    r.y = r.y - r.height;  // moves the y coordinate to the bottom one
  }
  return r;
}
rectangle intersection(rectangle r1, rectangle r2) {
  int x1_end, y1_end, x2_end, y2_end, intersecting_xr_end,
      intersecting_yr_end;  // coordinates of the top-right corner
  rectangle intersecting_r;

  //Canocolize first!!!
  r1 = canonicalize(r1);
  r2 = canonicalize(r2);

  // Calculate the coodinates of the top-right corner
  x1_end = r1.x + r1.width;
  y1_end = r1.y + r1.height;

  x2_end = r2.x + r2.width;
  y2_end = r2.y + r2.height;

  // Check for non-intersecting rcetangles first
  if (((x2_end < r1.x) || (x1_end < r2.x)) || ((r2.y > y1_end) || (r1.y > y2_end))) {
    intersecting_r.width = 0;
    intersecting_r.height = 0;
    return intersecting_r;  // so the rest of the code in this function doesn't run
  }

  // Calculate the bottom-left coordinate of the intersecting recttangle
  intersecting_r.x = (r1.x > r2.x) ? r1.x : r2.x;
  intersecting_r.y = (r1.y > r2.y) ? r1.y : r2.y;

  // Calculate the top-right coordinate of the intersecting rectangle
  intersecting_xr_end = (x1_end < x2_end) ? x1_end : x2_end;
  intersecting_yr_end = (y1_end < y2_end) ? y1_end : y2_end;

  // Calculate the width and height of intersecting rectangle
  intersecting_r.width = max(intersecting_r.x, intersecting_xr_end) -
                         min(intersecting_r.x, intersecting_xr_end);
  intersecting_r.height = max(intersecting_r.y, intersecting_yr_end) -
                          min(intersecting_r.y, intersecting_yr_end);

  return intersecting_r;
}

//You should not need to modify any code below this line
void printRectangle(rectangle r) {
  r = canonicalize(r);
  if (r.width == 0 && r.height == 0) {
    printf("<empty>\n");
  }
  else {
    printf("(%d,%d) to (%d,%d)\n", r.x, r.y, r.x + r.width, r.y + r.height);
  }
}

int main(void) {
  rectangle r1;
  rectangle r2;
  rectangle r3;
  rectangle r4;

  r1.x = 2;
  r1.y = 3;
  r1.width = 5;
  r1.height = 6;
  printf("r1 is ");
  printRectangle(r1);

  r2.x = 4;
  r2.y = 5;
  r2.width = -5;
  r2.height = -7;
  printf("r2 is ");
  printRectangle(r2);
  r3.x = -2;
  r3.y = 7;
  r3.width = 7;
  r3.height = -10;
  printf("r3 is ");
  printRectangle(r3);

  r4.x = 0;
  r4.y = 7;
  r4.width = -4;
  r4.height = 2;
  printf("r4 is ");
  printRectangle(r4);

  //test everything with r1
  rectangle i = intersection(r1, r1);
  printf("intersection(r1,r1): ");
  printRectangle(i);

  i = intersection(r1, r2);
  printf("intersection(r1,r2): ");
  printRectangle(i);

  i = intersection(r1, r3);
  printf("intersection(r1,r3): ");
  printRectangle(i);

  i = intersection(r1, r4);
  printf("intersection(r1,r4): ");
  printRectangle(i);

  //test everything with r2
  i = intersection(r2, r1);
  printf("intersection(r2,r1): ");
  printRectangle(i);

  i = intersection(r2, r2);
  printf("intersection(r2,r2): ");
  printRectangle(i);

  i = intersection(r2, r3);
  printf("intersection(r2,r3): ");
  printRectangle(i);

  i = intersection(r2, r4);
  printf("intersection(r2,r4): ");
  printRectangle(i);

  //test everything with r3
  i = intersection(r3, r1);
  printf("intersection(r3,r1): ");
  printRectangle(i);

  i = intersection(r3, r2);
  printf("intersection(r3,r2): ");
  printRectangle(i);

  i = intersection(r3, r3);
  printf("intersection(r3,r3): ");
  printRectangle(i);

  i = intersection(r3, r4);
  printf("intersection(r3,r4): ");
  printRectangle(i);

  //test everything with r4
  i = intersection(r4, r1);
  printf("intersection(r4,r1): ");
  printRectangle(i);

  i = intersection(r4, r2);
  printf("intersection(r4,r2): ");
  printRectangle(i);

  i = intersection(r4, r3);
  printf("intersection(r4,r3): ");
  printRectangle(i);

  i = intersection(r4, r4);
  printf("intersection(r4,r4): ");
  printRectangle(i);

  return EXIT_SUCCESS;
}
