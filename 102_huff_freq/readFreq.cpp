#include "readFreq.h"

#include <stdio.h>

#include <cstdlib>
#include <fstream>
void printSym(std::ostream & s, unsigned sym) {
  if (sym > 256) {
    s << "INV";
  }
  else if (sym == 256) {
    s << "EOF";
  }
  else if (isprint(sym)) {
    char c = sym;
    s << "'" << c << "'";
  }
  else {
    std::streamsize w = s.width(3);
    s << std::hex << sym << std::dec;
    s.width(w);
  }
}
uint64_t * readFrequencies(const char * fname) {
  std::fstream inputFile;
  inputFile.open(fname, std::fstream::in);
  if (inputFile.fail()) {
    std::cerr << "Invalid file name\n";
    exit(EXIT_FAILURE);
  }
  uint64_t * freqArray = new uint64_t[257];

  // initializa array as zero
  for (size_t i = 0; i < 257; i++) {
    freqArray[i] = 0;
  }

  size_t readChar = 256;  // EOF

  do {
    freqArray[readChar]++;
    readChar = inputFile.get();
  } while (!inputFile.eof());

  inputFile.close();
  return freqArray;
}
