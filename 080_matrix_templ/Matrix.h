#ifndef __T_MATRIX_H___
#define __T_MATRIX_H___

#include <assert.h>

#include <cstdlib>
#include <iostream>
#include <vector>

template<typename T>
class Matrix {
 private:
  int numRows;
  int numColumns;
  std::vector<std::vector<T> > rows;

 public:
  Matrix() : numRows(0), numColumns(0) {}
  Matrix(int r, int c) : numRows(r), numColumns(c) {
    rows.resize(r);
    typename std::vector<std::vector<T> >::iterator it = rows.begin();
    while (it != rows.end()) {
      (*it).resize(c);
      ++it;
    }
  }

  Matrix(const Matrix & rhs) {
    numRows = rhs.numRows;
    numColumns = rhs.numColumns;
    rows = rhs.rows;
  }

  ~Matrix() { rows.clear(); }

  Matrix & operator=(const Matrix & rhs) {
    if (this != &rhs) {
      std::vector<std::vector<T> > temp = rhs.rows;
      rows.clear();
      rows = temp;
      numRows = rhs.numRows;
      numColumns = rhs.numColumns;
    }
    return *this;
  }
  int getRows() const { return numRows; }
  int getColumns() const { return numColumns; }
  const std::vector<T> & operator[](int index) const {
    assert(index >= 0 && index < numRows);
    return rows[index];
  }
  std::vector<T> & operator[](int index) {
    assert(index >= 0 && index < numRows);
    return rows[index];
  }

  bool operator==(const Matrix & rhs) const {
    if (rows != rhs.rows) {
      return false;
    }
    else {
      return true;
    }
  }

  Matrix operator+(const Matrix & rhs) const {
    assert(numRows == rhs.numRows && numColumns == rhs.numColumns);
    Matrix result(numRows, numColumns);
    for (int i = 0; i < numRows; i++) {
      for (int j = 0; j < numColumns; j++) {
        result[i][j] = rows[i][j] + rhs.rows[i][j];
      }
    }

    return result;
  }

  template<typename B>
  friend std::ostream & operator<<(std::ostream & s, const Matrix<B> & rhs);
};

template<typename B>
std::ostream & operator<<(std::ostream & s, const Matrix<B> & rhs) {
  s << "[ ";
  for (int i = 0; i < rhs.getRows(); i++) {
    s << "{";
    for (size_t j = 0; j < rhs.rows[i].size(); j++) {
      if (j > 0) {
        s << ", ";
      }
      s << rhs.rows[i][j];
    }
    s << "}";
    if (i != rhs.getRows() - 1) {
      s << ",\n";
    }
  }
  s << " ]";
  return s;
}

#endif
