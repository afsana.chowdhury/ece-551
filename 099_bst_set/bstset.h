#include <iostream>

#include "set.h"
template<typename T>
class Node {
 public:
  T data;
  Node * left;
  Node * right;

 public:
  Node(T val) : data(val), left(NULL), right(NULL) {}
  ~Node(){};
};

template<typename T>
class BstSet : public Set<T> {
  Node<T> * root;

 public:
  virtual void add(const T & key) {
    Node<T> ** current = &root;
    while (*current != NULL) {
      if (key < ((*current)->data)) {
        current = &(*current)->left;
      }
      else if (key > ((*current)->data)) {
        current = &(*current)->right;
      }
      else {  // data already exists - do nothing
        return;
      }
    }
    *current = new Node<T>(key);
  }
  virtual bool contains(const T & key) const {
    Node<T> * current = root;
    while (current != NULL) {
      if (current->data == key) {
        return true;
      }
      else if (current->data > key) {
        current = current->left;
      }
      else {
        current = current->right;
      }
    }

    return false;
  }
  virtual void remove(const T & key) { root = remove(key, root); }
  virtual Node<T> * remove(const T & key, Node<T> * curr) {
    if (curr != NULL) {
      if (key < curr->data) {
        curr->left = remove(key, curr->left);
      }
      else if (key > curr->data) {
        curr->right = remove(key, curr->right);
      }
      else {
        curr = do_remove(curr);
      }
    }
    return curr;
  }
  Node<T> * do_remove(Node<T> * curr) {
    // node has no child
    if (curr->left == NULL && curr->right == NULL) {
      delete curr;
      return NULL;
    }
    // node with only one child or no child
    else if (curr->left == NULL) {
      Node<T> * temp = curr->right;
      delete curr;
      return temp;
    }
    else if (curr->right == NULL) {
      Node<T> * temp = curr->left;
      delete curr;
      return temp;
    }
    else {  // no child is null
      Node<T> * replaceWith = curr->left;
      while (replaceWith->right != NULL) {
        replaceWith = replaceWith->right;
      }
      T savedData = replaceWith->data;
      remove(replaceWith->data);
      curr->data = savedData;
      return curr;
    }
  }

  BstSet() : root(NULL) {}

  virtual ~BstSet<T>() { destroy(root); }

  void destroy(Node<T> * current) {
    if (current != NULL) {
      destroy(current->left);
      destroy(current->right);
      delete current;
    }
  }

  BstSet(const BstSet & rhs) : root(NULL) { traversePreOrder(rhs.root); }
  BstSet & operator=(const BstSet & rhs) {
    if (this != &rhs) {
      BstSet<T> temp(rhs);

      Node<T> * oldRoot = root;
      root = temp.root;
      temp.root = NULL;
      destroy(oldRoot);
    }
    return *this;
  }

  void traversePreOrder(Node<T> * current) {
    if (current != NULL) {
      traversePreOrder(current->left);
      add(current->data);
      traversePreOrder(current->right);
    }
  }
  void inOrder() { traverseInOrder(root); }
  void traverseInOrder(Node<T> * current) {
    if (current != NULL) {
      traverseInOrder(current->left);
      std::cout << current->data << " ";
      traverseInOrder(current->right);
    }
  }
};
