#include "bstset.h"

#include <cstdlib>
#include <iostream>

int main() {
  BstSet<int> bstset;
  bstset.add(1);
  bstset.add(3);
  bstset.add(-1);
  bstset.add(-2);
  bstset.add(0);
  bstset.add(6);
  bstset.add(6);
  bstset.add(5);
  bstset.inOrder();
  std::cout << "\n";

  bstset.add(7);
  bstset.add(4);

  BstSet<int> bstmap2(bstset);  // copy
  std::cout << "MAP-2\n";
  bstmap2.inOrder();
  std::cout << "\n";

  bstset.remove(1);  // root
  bstset.remove(5);  // no child
  bstset.inOrder();
  if (bstset.contains(1)) {
    std::cout << " contains 1\n";
  }
  else {
    std::cout << " does not contain 1\n";
  }
  if (bstset.contains(0)) {
    std::cout << " contains 0\n";
  }
  else {
    std::cout << " does not contain 0\n";
  }
  bstset.inOrder();
  std::cout << "\n";
  bstset.remove(3);  // one chile
  bstset.inOrder();
  std::cout << "\n";
  bstset.remove(4);  // one chile
  bstset.inOrder();
  std::cout << "\n";
  bstset.remove(6);  // one chile
  bstset.inOrder();
  std::cout << "\n";

  bstmap2 = bstset;  // copy
  std::cout << "MAP-2\n";
  bstmap2.inOrder();
  std::cout << "\n";

  return EXIT_SUCCESS;
}
