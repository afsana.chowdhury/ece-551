#include "outname.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * computeOutputFileName(const char * inputName) {
  if (inputName == NULL) {
    fprintf(stderr, "Error in inputName pointer\n");
    exit(EXIT_FAILURE);
  }

  char * outputName = malloc((strlen(inputName) + 1) * sizeof(*outputName));
  stpcpy(outputName, inputName);
  char * suffix = ".counts";
  outputName = realloc(outputName,
                       (strlen(outputName) + strlen(suffix) + 1) * sizeof(*outputName));
  outputName = strcat(outputName, suffix);

  return outputName;
}
