#include <ctype.h>
#include <stdio.h>
#include <string.h>

#include <cstdlib>
#include <iostream>
#include <sstream>
class Expression {
 public:
  virtual std::string toString() const = 0;  //abstract method
  virtual ~Expression() {}
};

class NumExpression : public Expression {
  long number;

 public:
  explicit NumExpression(long inputNumber) : number(inputNumber) {}
  virtual std::string toString() const {
    std::stringstream ss;
    ss << number;
    return ss.str();
  }

  ~NumExpression() {}
};

class OpExpression : public Expression {
  Expression & lhs;
  Expression & rhs;
  char op;

 public:
  explicit OpExpression(Expression * lhs, Expression * rhs, char op) :
      lhs(*lhs),
      rhs(*rhs),
      op(op) {}
  virtual std::string toString() const {
    std::stringstream ss;
    ss << "(" << lhs.toString() << " " << op << " " << rhs.toString() << ")";
    return ss.str();
  }
  ~OpExpression() {
    delete &lhs;
    delete &rhs;
  }
};

class PlusExpression : public OpExpression {
 public:
  explicit PlusExpression(Expression * lhs, Expression * rhs) :
      OpExpression(lhs, rhs, '+') {}
};

class MinusExpression : public OpExpression {
 public:
  explicit MinusExpression(Expression * lhs, Expression * rhs) :
      OpExpression(lhs, rhs, '-') {}
};

class TimesExpression : public OpExpression {
 public:
  explicit TimesExpression(Expression * lhs, Expression * rhs) :
      OpExpression(lhs, rhs, '*') {}
};

class DivExpression : public OpExpression {
 public:
  explicit DivExpression(Expression * lhs, Expression * rhs) :
      OpExpression(lhs, rhs, '/') {}
};
