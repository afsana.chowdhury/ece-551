#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

// function protypes
int * arrayMax(int * array, int n);

void readFile(FILE * f, int * arrayAlphabet) {
  int c;
  while ((c = fgetc(f)) != EOF) {
    if (isalpha(c)) {
      c = tolower(c);
      c -= 'a';
      arrayAlphabet[c] += 1;  // adding 1 to the counting of that letter
    }
  }
}

int main(int argc, char ** argv) {
  if (argc != 2) {  // program name and file name
    fprintf(stderr, "Usage: breaker inputFileName\n");
    return EXIT_FAILURE;
  }
  FILE * f = fopen(argv[1], "r");
  if (f == NULL) {
    perror("Could not open file");
    return EXIT_FAILURE;
  }

  int arrayAlphabet[26] = {0};  // each index representing alphabets a-z

  readFile(f, arrayAlphabet);

  int * mostFrequent = arrayMax(arrayAlphabet, 26);

  if (mostFrequent == NULL) {
    fprintf(stderr, "Error: More than one letters with highest freq\n");
    if (fclose(f) != 0) {
      perror("Failed to close the input file!");
      return EXIT_FAILURE;
    }
    return EXIT_FAILURE;
  }

  else {
    int key = (mostFrequent - arrayAlphabet);
    key -= 4;  // 'e' is in index 4
    if (key < 0)
      key += 26;
    fprintf(stdout, "%d\n", key);
  }

  if (fclose(f) != 0) {
    perror("Failed to close the input file!");
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}
