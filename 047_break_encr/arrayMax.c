#include <stdio.h>
#include <stdlib.h>

int * arrayMax(int * array, int n) {
  int largest;
  int frequency;

  if (n == 0)
    return NULL;

  largest = 0;    // 1st element of array
  frequency = 1;  // how many times the largest # appears

  for (int i = 1; i < n; i++) {  // start from the second element
    if (array[i] > array[largest]) {
      largest = i;
      frequency = 1;  // resetting
    }
    else if (array[i] == array[largest]) {
      frequency++;
    }
  }

  if (frequency != 1) {
    return NULL;
  }

  return &array[largest];
}
