Grading at 12/03/2021:02:41:50.016637
For commit ID 7c85b559fcf2f22729f2bf3e5269f698e3189444
Grading at Thu Dec  2 21:41:18 EST 2021
compiling
g++ -ggdb3 -Wall -Werror -pedantic -std=gnu++98 -o compress  bitstring.o  buildTree.o  compress.o  node.o  readFreq.o
Make succeed, compress created
Testcase 1
-----------------------
  - Valgrind was clean (no errors, no memory leaks)
Decompressing, and comparing to original...:
Your file matched the expected output
Testcase passed 
Testcase 2
-----------------------
  - Valgrind was clean (no errors, no memory leaks)
Decompressing, and comparing to original...:
Your file matched the expected output
Testcase passed 
Testcase 3
-----------------------
  - Valgrind was clean (no errors, no memory leaks)
Decompressing, and comparing to original...:
Your file matched the expected output
Testcase passed 

Overall Grade: A
