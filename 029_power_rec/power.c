unsigned power_helper(unsigned base, unsigned exp, unsigned ans) {
  if (exp == 0) {
    return ans;
  }
  else {
    return power_helper(base, exp - 1, ans * base);
  }
}

unsigned power(unsigned x, unsigned y) {
  return power_helper(x, y, 1);
}
