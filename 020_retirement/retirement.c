#include <stdio.h>
#include <stdlib.h>

struct _retire_info {
  int months;  // number of months it is applicable to
  double
      contribution;  // how many dollars are contributed (or spent if negative) from the account per month
  double
      rate_of_return;  // rate of returns (which we will asssume to be "after inflation")
};
typedef struct _retire_info retire_info;

double calculate_balance(int age, double balance, retire_info info) {
  int age_years = age / 12;
  int age_months = age % 12;
  double monthly_rate_of_return = info.rate_of_return / (100 * 12);
  printf("Age %3d month %2d you have $%.2lf\n", age_years, age_months, balance);
  balance = balance + (balance * monthly_rate_of_return) + info.contribution;
  return balance;
}

void retirement(int startAge,         //in months
                double initial,       //initial savings in dollars
                retire_info working,  //info about working
                retire_info retired)  //info about being retired
{
  int age = startAge;
  double balance = initial;

  for (int i = 0; i < working.months; i++) {
    balance = calculate_balance(age, balance, working);
    age = age + 1;
  }

  for (int j = 0; j < retired.months; j++) {
    balance = calculate_balance(age, balance, retired);
    age = age + 1;
  }
}

int main(void) {
  retire_info working, retired;
  int startAge = 327;
  double initial = 21345;

  working.months = 489;
  working.contribution = 1000;
  working.rate_of_return = 4.5;  //% per year

  retired.months = 384;
  retired.contribution = -4000;
  retired.rate_of_return = 1;

  retirement(startAge, initial, working, retired);

  return EXIT_SUCCESS;
}
