#include "Story.hpp"

#include <algorithm>
#include <cerrno>
#include <climits>

//********************************************************************
// Function Name: readPages
// Input: None
// Returns: Nothing
//
// Description : This function reads all the pages in the story/directory
// and make sure if they are all valid
// Access Type: public
//********************************************************************
void Story::readPages() throw(invalid_page) {
  size_t pageNumber = 1;
  bool finishedReadingPages = false;
  do {
    std::stringstream pageFile;
    std::string pageFileName;
    pageFile << storyDirectory << "/page" << pageNumber << ".txt";
    pageFileName = pageFile.str();
    Page * pg = new Page(pageFileName, pageNumber);
    if (!pg->openPage()) {  // error opening page
      delete pg;
      if (pageNumber == 1) {
        break;
      }
      else {
        finishedReadingPages = true;
      }
    }
    else {
      try {
        pg->readPage();
      }
      catch (invalid_page & ip) {
        delete pg;
        throw;
      }
      pagesArr.push_back(pg);
      pageNumber++;
    }
  } while (!finishedReadingPages);
  if (pageNumber == 1) {
    std::cerr << "Error: Missing page1.txt\n";
    exit(EXIT_FAILURE);
  }
  totalPages = pageNumber - 1;
}

//********************************************************************
// Function Name: printPages
// Input: None
// Returns: Nothing
//
// Description : This function prints all the read pages
// Access Type: public (used in testing)
//********************************************************************
void Story::printPages() const {
  for (std::vector<Page *>::const_iterator it = pagesArr.begin(); it != pagesArr.end();
       ++it) {
    (*it)->printPage();
  }
}

//********************************************************************
// Function Name: verifyStoryNavigations
// Input: None
// Returns: Nothing
//
// Description : This function verifies if the navigations options
// of all the pages in the story is valid
// Access Type: public
//********************************************************************
void Story::verifyStoryNavigations() const {
  bool foundWinCase = false;
  bool foundLoseCase = false;
  for (std::vector<Page *>::const_iterator it = pagesArr.begin(); it != pagesArr.end();
       ++it) {
    // 4a. Every page that is referenced by a choice is valid.
    (*it)->verifyPageNavigations(totalPages);

    // 4b. Every page (except page 1) is referenced by at least one other page's choices.

    // First, get the list of pages that "this" page refers to
    std::vector<size_t> referencedPages = (*it)->getNavigationPageNumber();

    // Now for each page in the list, add "this" page's number in the referencedBy list
    for (std::vector<size_t>::iterator it_referred = referencedPages.begin();
         it_referred != referencedPages.end();
         ++it_referred) {
      size_t pageNum = *it_referred;

      // 0 is for WIN and LOSE AND "this" page needs to be referred by OTHER page
      if (pageNum != 0 && pageNum != (*it)->getPageNumber()) {
        pagesArr[pageNum - 1]->addReferredBy((*it)->getPageNumber());
      }
      else {
        if ((*it)->getPageType() == WIN) {
          foundWinCase = true;
        }
        else if ((*it)->getPageType() == LOSE) {
          foundLoseCase = true;
        }
      }
    }
  }
  // verify no referencedBy list is empty (except page 1)
  verifyReferences();

  // Verify: 4c. At least one page must be a WIN page
  // and at least one page must be a LOSE page.
  if (!foundWinCase) {
    throw invalid_page("No WIN case!");
  }
  if (!foundLoseCase) {
    throw invalid_page("No LOSE case!");
  }
}

//********************************************************************
// Function Name: verifyReferences
// Input: None
// Returns: Nothing
//
// Description : This function verifies if each page in the story
// is referred by at least one other page
// Access Type: private
//********************************************************************
void Story::verifyReferences() const {
  // for each page, verify the list of referencedBy is not empty (except page 1)
  for (std::vector<Page *>::const_iterator it = pagesArr.begin(); it != pagesArr.end();
       ++it) {
    if (!(*it)->checkPageReference()) {  // empty refence list (except page 1)
      throw invalid_page("Error: Page not referred by any other page!");
    }
  }
}

//********************************************************************
// Function Name: createYourStory
// Input: None
// Returns: Nothing
//
// Description : This function creates the story by showing the pages
// and taking user input for navigation
// Access Type: public
//********************************************************************
void Story::createYourStory() const {
  size_t pageNum = 1;
  while (true) {
    pagesArr[pageNum - 1]->printPage();
    pageType_t currentPageType = pagesArr[pageNum - 1]->getPageType();
    if (currentPageType == WIN || currentPageType == LOSE) {
      return;
    }
    while (true) {
      std::string userInput;
      std::getline(std::cin, userInput);
      size_t nextPage = findNextPage(userInput, pageNum);
      if (nextPage == 0) {
        std::cout << "That is not a valid choice, please try again" << std::endl;
        continue;
      }
      else {
        pageNum = nextPage;
        break;
      }
    }
  }
}

//********************************************************************
// Function Name: findNextPage
// Input: None
// Returns: next page number (returns 0 for invalid option)
//
// Description : This function finds the next page number based on user input
// Access Type: private
//********************************************************************
size_t Story::findNextPage(std::string & userInput, size_t currentPageNumber) const {
  long int selectedOption;
  char * ptrAfterPageNum;
  size_t nextPageNumber;
  selectedOption = std::strtol(userInput.c_str(), &ptrAfterPageNum, 10);

  if (selectedOption <= 0 || (selectedOption == LONG_MAX && errno == ERANGE) ||
      (*ptrAfterPageNum != '\0')) {
    nextPageNumber = 0;  // invalid option
  }
  else {
    nextPageNumber = pagesArr[currentPageNumber - 1]->findNextPageNumber(selectedOption);
  }

  return nextPageNumber;
}

//********************************************************************
// Function Name: determineStoryDepth
// Input: None
// Returns: Nothing
//
// Description : This function calculates the depth of each page in
// the story (i.e. the minimum number of other pages that a user has to
// see while reading through the story before reaching this page
// Access Type: public
//********************************************************************
void Story::determineStoryDepth() {
  Page * from = pagesArr[0];  // starting page
  for (std::vector<Page *>::iterator it = pagesArr.begin() + 1; it != pagesArr.end();
       ++it) {
    size_t pageStoryDepth = getShortestPath(from, *it);
    (*it)->setStoryDepth(pageStoryDepth);
  }
}

//********************************************************************
// Function Name: updateWorkList
// Input: 1) pointer to a page 2) path so far 3) WorkList
// Returns: Nothing
//
// Description : This templated function is part of BFS and DFS algorithms
// Access Type: private
//********************************************************************
template<typename WorkList>
void Story::updateWorkList(Page * currentNode,
                           Path & currentPath,
                           WorkList & todo) const {
  std::vector<size_t> referencedPages = currentNode->getNavigationPageNumber();
  size_t option = 0;
  for (std::vector<size_t>::iterator it = referencedPages.begin();
       it != referencedPages.end();
       ++it) {
    size_t nextPageIndex = (*it) - 1;
    option++;

    // update option of previous page
    std::pair<Page *, size_t> previousPage = currentPath.back();
    currentPath.pop_back();
    previousPage.second = option;
    currentPath.push_back(previousPage);

    // now add the neighbour to the path
    currentPath.push_back(std::make_pair(pagesArr[nextPageIndex], 0));
    todo.push(currentPath);
    currentPath.pop_back();  // go back to original currentPath
  }
}

//********************************************************************
// Function Name: getShortestPath
// Input: pointers to pages FROM and TO
// Returns: minimum number of pages the user has to go through
// to go from "from" page to "to" page
//
// Description : This function uses Breadth First Search (BFS)
// to find the shortest path between two pages
// Access Type: private
//********************************************************************
size_t Story::getShortestPath(Page * from, Page * to) const {
  PathQueue todo;               // paths
  std::vector<size_t> visited;  // pages

  Path currentPath;
  currentPath.push_back(std::make_pair(from, 0));
  todo.push(currentPath);

  Page * currentNode;
  size_t storyDepth = 0;

  while (!todo.empty()) {
    currentPath = todo.front();
    todo.pop();

    currentNode = currentPath.back().first;  //lastNode
    if (currentNode == to) {
      storyDepth = currentPath.size() - 1;
      break;  // found the shortest path
    }

    // if currentNode is NOT in visited
    if (std::find(visited.begin(), visited.end(), currentNode->getPageNumber()) ==
        visited.end()) {
      visited.push_back(currentNode->getPageNumber());

      if (currentNode->getPageType() != REGULAR) {  // win or lose
        continue;                                   // no reference pages
      }

      updateWorkList<PathQueue>(currentNode, currentPath, todo);
    }
  }
  return storyDepth;
}

//********************************************************************
// Function Name: printStoryDepth
// Input: None
// Returns: Nothing
//
// Description : This function prints the story depths of each page
// in the story
// Access Type: public
//********************************************************************
void Story::printStoryDepth() const {
  for (std::vector<Page *>::const_iterator it = pagesArr.begin(); it != pagesArr.end();
       ++it) {
    std::cout << "Page " << (*it)->getPageNumber();

    size_t pageStoryDepth = (*it)->getStoryDepth();
    if (pageStoryDepth == 0 && (*it)->getPageNumber() != 1) {
      std::cout << " is not reachable" << std::endl;
    }
    else {
      std::cout << ":" << pageStoryDepth << std::endl;
    }
  }
}

//********************************************************************
// Function Name: determineWaysToWin
// Input: None
// Returns: Nothing
//
// Description : This function finds all cycle-free ways to "win" a
// given choose your own adventure story
// Access Type: public
//********************************************************************
void Story::determineWaysToWin() {
  //run DFS from page-1 to all "WIN" pages
  Page * from = pagesArr[0];  // starting page
  getAllPaths(from);
}

//********************************************************************
// Function Name: determineWaysToWin
// Input: None
// Returns: Nothing
//
// Description : This function prints all cycle-free ways to "win" a
// given choose your own adventure story
// Access Type: public
//********************************************************************
void Story::printWaysToWin() const {
  if (winningPaths.empty()) {
    std::cout << "This story is unwinnable!" << std::endl;
    return;
  }
  for (std::list<Path>::const_iterator it1 = winningPaths.begin();
       it1 != winningPaths.end();
       ++it1) {
    Path currentPath = *it1;
    for (Path::const_iterator it2 = currentPath.begin(); it2 != currentPath.end();
         ++it2) {
      std::cout << (*it2).first->getPageNumber() << "(";
      size_t option = (*it2).second;
      if (option == 0) {
        std::cout << "win)" << std::endl;
      }
      else {
        std::cout << option << "),";
      }
    }
  }
}

//********************************************************************
// Function Name: getAllPaths
// Input: pointer to "from" page (Note: this will be page-1 most, if
// not all, of the time, but I still added it as an arguement to have
// some flexibility in future if needed
// Returns: Nothing
//
// Description : This function uses Depth First Search (DFS) to find
// all possible cycle-free ways to win from "from" page
// Access Type: private
//********************************************************************
void Story::getAllPaths(Page * from) {
  PathStack todo;               // paths
  std::vector<size_t> visited;  // pages

  Path currentPath;
  currentPath.push_back(std::make_pair(from, 0));
  todo.push(currentPath);

  Page * currentNode;

  while (!todo.empty()) {
    currentPath = todo.top();
    todo.pop();

    currentNode = currentPath.back().first;  //lastNode

    // if currentNode is NOT in visited
    if (std::find(visited.begin(), visited.end(), currentNode->getPageNumber()) ==
        visited.end()) {
      visited.push_back(currentNode->getPageNumber());

      if (currentNode->getPageType() != REGULAR) {  // win or lose
        if (currentNode->getPageType() == WIN) {
          winningPaths.push_back(currentPath);
        }
        continue;  // no reference pages
      }
      updateWorkList<PathStack>(currentNode, currentPath, todo);
    }
  }
}
