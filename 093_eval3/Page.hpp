#ifndef PAGE_HEADER
#define PAGE_HEADER

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

enum pageType_t { REGULAR = 0, WIN, LOSE };

class invalid_page : public std::exception {
  const char * err_msg;

 public:
  invalid_page(const std::string msg) : err_msg(msg.c_str()) {}
  invalid_page(const char * msg) : err_msg(msg) {}
  virtual const char * what() const throw() { return err_msg; }
};

class Page {
 private:
  const char * fileName;
  std::ifstream inputPage;
  size_t myPageNumber;
  pageType_t pageType;
  std::vector<std::pair<size_t, std::string> > navigationOptions;
  std::stringstream storyText;
  std::vector<size_t>
      referencedBy;  // list of pages with "this" page in navigation choice
  size_t storyDepth;

  void closePage();

  void readNavigations() throw(invalid_page);
  bool addNavigation(std::string & line) throw(invalid_page);
  void addPair(size_t page, const char * text);
  bool checkAddPageNavigation(const std::string & line) throw(invalid_page);
  void readText();

  void printNavigations() const;
  void printText() const;

 public:
  explicit Page(const char * file) :
      fileName(file),
      myPageNumber(0),
      pageType(REGULAR),
      storyDepth(0){};  // 0 is valid only for page1

  explicit Page(const std::string & file, size_t pageNumber) :
      fileName(file.c_str()),
      myPageNumber(pageNumber),
      pageType(REGULAR),
      storyDepth(0){};  // 0 is valid only for page1

  ~Page(){};

  bool openPage();
  void readPage() throw(invalid_page);
  void printPage() const;

  void verifyPageNavigations(size_t totalStoryPages) const throw(invalid_page);
  std::vector<size_t> getNavigationPageNumber() const;

  void addReferredBy(size_t referredByPage);
  bool checkPageReference() const;

  size_t findNextPageNumber(size_t nextPageNumber) const;
  void setStoryDepth(size_t pageStoryDepth);

  // get methods
  size_t getPageNumber() const { return myPageNumber; };
  pageType_t getPageType() const { return pageType; };
  size_t getStoryDepth() const { return storyDepth; };
};

#endif
