#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>

#include "Story.hpp"
int main(int argc, char ** argv) {
  if (argc != 2) {
    std::cerr << "Please fix the arguments!\n";
    exit(EXIT_FAILURE);
  }

  Story story(argv[1]);
  try {
    story.readPages();
    story.verifyStoryNavigations();
    story.determineWaysToWin();
    story.printWaysToWin();
  }
  catch (invalid_page & ip) {
    std::cerr << ip.what() << std::endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
