#include <cstdlib>
#include <fstream>
#include <iostream>

#include "Page.hpp"
int main(int argc, char ** argv) {
  if (argc != 2) {
    std::cerr << "Please fix the arguments!\n";
    exit(EXIT_FAILURE);
  }

  Page pg(argv[1]);
  try {
    pg.openPage();
    pg.readPage();
  }
  catch (invalid_page & ip) {
    std::cerr << ip.what() << std::endl;
    return EXIT_FAILURE;
  }
  pg.printPage();

  return EXIT_SUCCESS;
}
