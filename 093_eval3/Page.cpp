#include "Page.hpp"

#include <cerrno>
#include <climits>
#include <exception>

//********************************************************************
// Function Name: openPage
// Input: None
// Returns: True/False if it could successfully open the page
//
// Description : This function tries to open the file with the
// "fileName" attribute of this page
// Access Type: public (since Story needs to call it)
//********************************************************************
bool Page::openPage() {
  bool success = true;
  inputPage.open(fileName, std::fstream::in);
  if (inputPage.fail()) {
    success = false;
  }
  return success;
}

//********************************************************************
// Function Name: closePage
// Input: None
// Returns: Nothing
//
// Description: This function simply closes the "inputPage" file
// Access Type: private
//********************************************************************
void Page::closePage() {
  inputPage.close();
}

//********************************************************************
// Function Name: readPage
// Input: None
// Returns: Nothing
//
// Description: This function reads the page (i.e. saves the navigation
// options and the text) and closes the file when it's done
// Access Type: public (since Story needs to call it)
//********************************************************************
void Page::readPage() throw(invalid_page) {
  try {
    readNavigations();
    readText();
    closePage();
  }
  catch (invalid_page & ip) {
    closePage();
    throw;
  }
}

//********************************************************************
// Function Name: readNavigations
// Input: None
// Returns: Nothing
//
// Description: This function reads the navigation options (including
// if it's WIN and LOSE) of this page
// Access Type: private
//********************************************************************
void Page::readNavigations() throw(invalid_page) {
  bool endOfNavigation = false;
  while (inputPage.good() && !endOfNavigation) {
    std::string navigation;
    std::getline(inputPage, navigation);
    endOfNavigation = addNavigation(navigation);
  }
}
//********************************************************************
// Function Name: addNavigation
// Input: line from the file/page
// Returns: True/False if it has reached the end of navigations
// (i.e. beginning of the text portion of the page)
//
// Description: This function parses a line from the page to get
// navigation option from this page. It also checks if this page is
// a WIN or a LOSE page and changes the pageType accordingly
// Access Type: private
//********************************************************************
bool Page::addNavigation(std::string & line) throw(invalid_page) {
  // first check if it's WIN or LOSE
  if (line == "WIN") {
    addPair(0, "Congratulations! You have won. Hooray!");
    pageType = WIN;
    return false;
  }
  else if (line == "LOSE") {
    addPair(0, "Sorry, you have lost. Better luck next time!");
    pageType = LOSE;
    return false;
  }
  else if (line[0] == '#') {
    if (navigationOptions.size() == 0) {  // if this is the first line
      throw invalid_page("No navigation provided!");
    }
    return true;
  }

  // Now check if the line has valid formatting and add it
  return checkAddPageNavigation(line);
}
//********************************************************************
// Function Name: addPair
// Input: navigation page number, navigation text to display
// Returns: Nothing
//
// Description: This function reads the navigation options (including
// if it's WIN and LOSE) of this page
// Access Type: private
//********************************************************************
void Page::addPair(size_t page, const char * text) {
  std::pair<size_t, std::string> navigation(page, text);
  navigationOptions.push_back(navigation);
}

//********************************************************************
// Function Name: checkAddPageNavigation
// Input: line from the page/file
// Returns: True/False if it has reached the end of navigations
// (i.e. beginning of the text portion of the page)
//
// Description: This function checks if a navigation line is valid
// (correct formatting) and adds the navigation option to the page
// Access Type: private
//********************************************************************
bool Page::checkAddPageNavigation(const std::string & line) throw(invalid_page) {
  // e.g. 47:Try to sneak past the sleeping dragon
  long int pageNumber;
  char * ptrAfterPageNum;
  pageNumber = std::strtol(line.c_str(), &ptrAfterPageNum, 10);

  if (pageNumber <= 0 || (pageNumber == LONG_MAX && errno == ERANGE)) {
    throw invalid_page("Error in navigation page number format!");
  }
  else if (*ptrAfterPageNum != ':') {
    throw invalid_page("Error in navigation format! Possibly missing colon.");
  }
  else {  // correct formatting
    addPair(pageNumber, ++ptrAfterPageNum);
    return false;
  }
}

//********************************************************************
// Function Name: printNavigations
// Input: None
// Returns: Nothing
//
// Description: This function prints the navigation options of a page
// in required formatting
// Access Type: private
//********************************************************************
void Page::printNavigations() const {
  if (navigationOptions.front().first == 0) {  // WIN or LOSE
    std::cout << std::endl << navigationOptions.front().second << std::endl;
    return;
  }

  std::cout << "\nWhat would you like to do?\n" << std::endl;
  size_t option = 1;
  for (std::vector<std::pair<size_t, std::string> >::const_iterator it =
           navigationOptions.begin();
       it != navigationOptions.end();
       ++it) {
    std::cout << " " << option++ << ". " << (*it).second << std::endl;
  }
}

//********************************************************************
// Function Name: readText
// Input: None
// Returns: Nothing
//
// Description: This function reads and saves the text portion of a page
// Access Type: private
//********************************************************************
void Page::readText() {
  std::string text;
  while (std::getline(inputPage, text)) {
    storyText << text << std::endl;
  }
}

//********************************************************************
// Function Name: printText
// Input: None
// Returns: Nothing
//
// Description: This function prints the text portion of a page
// Access Type: private
//********************************************************************
void Page::printText() const {
  std::cout << storyText.str();
}

//********************************************************************
// Function Name: printPage
// Input: None
// Returns: Nothing
//
// Description: This function prints the entire page in required formatting
// Access Type: public
//********************************************************************
void Page::printPage() const {
  printText();
  printNavigations();
}

//********************************************************************
// Function Name: verifyPageNavigations
// Input: number of total pages in the story
// Returns: Nothing
//
// Description: This function checks if the navigation option of the
// page is valid (if the page is in the story)
// Access Type: public (since Story needs to call it)
//********************************************************************
void Page::verifyPageNavigations(size_t totalStoryPages) const throw(invalid_page) {
  for (std::vector<std::pair<size_t, std::string> >::const_iterator it =
           navigationOptions.begin();
       it != navigationOptions.end();
       ++it) {
    if (it->first < 0 || it->first > totalStoryPages) {  // 0 for WIN or LOSE
      throw invalid_page("Error in navigation page number: Out of range!");
    }
  }
}

//********************************************************************
// Function Name: getNavigationPageNumber
// Input: None
// Returns: a vector of the pages referenced by this page
//
// Description: This function reads the saved navigation options
// and returns a vector of the referenced pages
// Access Type: public (since Story needs to call it)
//********************************************************************
std::vector<size_t> Page::getNavigationPageNumber() const {
  std::vector<size_t> referencedPages;
  for (std::vector<std::pair<size_t, std::string> >::const_iterator it =
           navigationOptions.begin();
       it != navigationOptions.end();
       ++it) {
    referencedPages.push_back(it->first);
  }
  return referencedPages;
}

//********************************************************************
// Function Name: addReferredBy
// Input: Number of the page that refers this page in the navigation
// Returns: a vector of the pages referenced by this pageNothing
//
// Description: This function reads the saved navigation options
// and returns a vector of the referenced pages
// Access Type: public (since Story needs to call it)
//********************************************************************
void Page::addReferredBy(size_t referredByPage) {
  referencedBy.push_back(referredByPage);
}

//********************************************************************
// Function Name: checkPageReference
// Input: None
// Returns: False if referencedBy list is empty (except page 1),
// true otherwise
//
// Description: This function checks if this page(except page 1) is
// referred by at least one other page
// Access Type: public (since Story needs to call it)
//********************************************************************
bool Page::checkPageReference() const {
  bool validPageReference = true;
  if (myPageNumber != 1 && referencedBy.size() == 0) {
    validPageReference = false;
  }
  return validPageReference;
}

//********************************************************************
// Function Name: findNextPageNumber
// Input: option selected by user
// Returns: the page number the selcted option refers to
//
// Description: This function gets the next page number from the
// navigation options of this page based on the selected option
// Access Type: public (since Story needs to call it)
//********************************************************************
size_t Page::findNextPageNumber(size_t selectedOption) const {
  size_t nextPageNumber = 0;
  if (selectedOption > 0 && selectedOption <= navigationOptions.size()) {
    nextPageNumber = navigationOptions[selectedOption - 1].first;
  }
  return nextPageNumber;
}

//********************************************************************
// Function Name: setStoryDepth
// Input: story depth of this page
// Returns: the page number the selcted option refers toNothing
//
// Description: This function sets the story depth of this page to the
// input value
// Access Type: public (since Story needs to call it)
//********************************************************************
void Page::setStoryDepth(size_t pageStoryDepth) {
  storyDepth = pageStoryDepth;
}
