#ifndef STORY_HEADER
#define STORY_HEADER

#include <list>
#include <queue>
#include <stack>

#include "Page.hpp"

typedef std::list<std::pair<Page *, size_t> > Path;  // Page and selected option
typedef std::queue<Path> PathQueue;
typedef std::stack<Path> PathStack;

class Story {
 private:
  std::string storyDirectory;
  std::vector<Page *> pagesArr;
  size_t totalPages;
  std::list<Path> winningPaths;

  void verifyReferences() const;
  size_t findNextPage(std::string & userInput, size_t currentPageNumber) const;

  size_t getShortestPath(Page * from, Page * to) const;  // BFS
  void getAllPaths(Page * from);                         // DFS

  template<typename WorkList>
  void updateWorkList(Page * currentNode, Path & currentPath, WorkList & todo) const;

 public:
  Story(const char * directory) : storyDirectory(directory), totalPages(0){};
  ~Story() {
    for (std::vector<Page *>::iterator it = pagesArr.begin(); it != pagesArr.end();
         ++it) {
      delete *it;
    }
  };
  void readPages() throw(invalid_page);
  void printPages() const;
  void verifyStoryNavigations() const;
  void createYourStory() const;

  void determineStoryDepth();
  void printStoryDepth() const;

  void determineWaysToWin();
  void printWaysToWin() const;
};

#endif
