#error
cyoa-step1 invalid1.txt
cyoa-step2 invalid1
cyoa-step2 invalid2
cyoa-step2 invalid3
cyoa-step2 invalid4
cyoa-step2 invalid5
cyoa-step2 invalid6
cyoa-step2 invalid7
cyoa-step2 invalid8
cyoa-step2 invalid9
cyoa-step2 invalid10

#success
cyoa-step1 story1/page1.txt
cyoa-step1 story1/page11.txt
cyoa-step1 page50.txt
cyoa-step2 story1 < test1/input1.txt
cyoa-step2 story1 < test1/input2.txt
cyoa-step2 story1 < test1/input3.txt
cyoa-step2 story2 < test2/input1.txt
cyoa-step3 story1
cyoa-step3 story2
cyoa-step3 story3
cyoa-step4 story1
cyoa-step4 story2
cyoa-step4 story3
cyoa-step4 story4
