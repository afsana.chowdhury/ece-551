#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>

template<typename T>
T & operator<<(T & stream, std::vector<std::string> & lineVector) {
  for (size_t i = 0; i < lineVector.size(); i++) {
    stream << lineVector[i] << std::endl;
  }
  return stream;
}

template<typename S>
void readAndSort(S & inStream, std::vector<std::string> & lineVector) {
  std::string line;
  std::getline(inStream, line);
  while (inStream.good()) {
    lineVector.push_back(line);
    std::getline(inStream, line);
  }
  std::sort(lineVector.begin(), lineVector.end());
  std::cout << lineVector;
}

int main(int argc, char ** argv) {
  std::vector<std::string> inputLines;
  if (argc == 1) {
    // read from stdin
    readAndSort(std::cin, inputLines);
  }
  else if (argc > 1) {
    // read form each file
    int numOfFiles = argc - 1;
    int fileToRead = 1;
    do {
      std::fstream inputFile;
      inputFile.open(argv[fileToRead], std::fstream::in);
      if (inputFile.fail()) {
        std::cerr << "Error opening file" << std::endl;
        return EXIT_FAILURE;
      }
      readAndSort(inputFile, inputLines);
      inputFile.close();
      inputLines.erase(inputLines.begin(), inputLines.end());
      fileToRead++;
    } while (fileToRead <= numOfFiles);
  }

  return EXIT_SUCCESS;
}
