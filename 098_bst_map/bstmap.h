#include <algorithm>
#include <iostream>
#include <vector>

#include "map.h"

template<typename K, typename V>
class Node {
 public:
  std::pair<K, V> keyValue;
  Node * left;
  Node * right;

 public:
  Node(K key, V value) : keyValue(std::pair<K, V>(key, value)), left(NULL), right(NULL) {}
  ~Node(){};
};

template<typename K, typename V>
class BstMap : public Map<K, V> {
  Node<K, V> * root;

 public:
  BstMap() : root(NULL) {}
  virtual void add(const K & key, const V & value) {
    Node<K, V> ** current = &root;
    while (*current != NULL) {
      if (key < ((*current)->keyValue).first) {
        current = &(*current)->left;
      }
      else if (key > ((*current)->keyValue).first) {
        current = &(*current)->right;
      }
      else {  // same key, replace value
        ((*current)->keyValue).second = value;
        return;
      }
    }
    *current = new Node<K, V>(key, value);
  }
  virtual const V & lookup(const K & key) const throw(std::invalid_argument) {
    Node<K, V> * current = root;
    while (current != NULL) {
      if ((current->keyValue).first == key) {
        return (current->keyValue).second;
      }
      else if ((current->keyValue).first > key) {
        current = current->left;
      }
      else {
        current = current->right;
      }
    }

    throw std::invalid_argument("key not found");
    return (root->keyValue).second;  // doesn't matter
  }

  virtual void remove(const K & key) { root = remove(key, root); }
  virtual Node<K, V> * remove(const K & key, Node<K, V> * curr) {
    if (curr != NULL) {
      if (key < (curr->keyValue).first) {
        curr->left = remove(key, curr->left);
      }
      else if (key > (curr->keyValue).first) {
        curr->right = remove(key, curr->right);
      }
      else {
        curr = do_remove(curr);
      }
    }
    return curr;
  }
  Node<K, V> * do_remove(Node<K, V> * curr) {
    // node has no child
    if (curr->left == NULL && curr->right == NULL) {
      delete curr;
      return NULL;
    }
    // node with only one child or no child
    else if (curr->left == NULL) {
      Node<K, V> * temp = curr->right;
      delete curr;
      return temp;
    }
    else if (curr->right == NULL) {
      Node<K, V> * temp = curr->left;
      delete curr;
      return temp;
    }
    else {  // no child is null
      Node<K, V> * replaceWith = curr->left;
      while (replaceWith->right != NULL) {
        replaceWith = replaceWith->right;
      }
      std::pair<K, V> savedKV = replaceWith->keyValue;
      remove((replaceWith->keyValue).first);
      /*
      Node<K, V> * toRemove = replaceWith;
      if (toRemove->left != NULL) {
        toRemove->right = curr->right;
        delete curr;
        return toRemove;
      }
      else {
        std::swap(curr->keyValue, toRemove->keyValue);
        delete toRemove;
        *replaceWith = NULL;
        return curr;
	}*/

      curr->keyValue = savedKV;
      return curr;
    }
  }

  BstMap(const BstMap & rhs) : root(NULL) { traversePreOrder(rhs.root); }
  BstMap & operator=(const BstMap & rhs) {
    if (this != &rhs) {
      BstMap<K, V> temp(rhs);

      Node<K, V> * oldRoot = root;
      root = temp.root;
      temp.root = NULL;
      destroy(oldRoot);
    }
    return *this;
  }
  virtual ~BstMap<K, V>() { destroy(root); }

  void destroy(Node<K, V> * current) {
    if (current != NULL) {
      destroy(current->left);
      destroy(current->right);
      delete current;
    }
  }
  void inOrder() { traverseInOrder(root); }
  void traverseInOrder(Node<K, V> * current) {
    if (current != NULL) {
      traverseInOrder(current->left);
      std::cout << current->keyValue.first << " ";
      traverseInOrder(current->right);
    }
  }

  void preOrder() { traversePreOrder(root); }
  void traversePreOrder(Node<K, V> * current) {
    if (current != NULL) {
      traversePreOrder(current->left);
      add(current->keyValue.first, current->keyValue.second);
      traversePreOrder(current->right);
    }
  }
};
