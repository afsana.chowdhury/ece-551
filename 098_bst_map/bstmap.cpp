#include "bstmap.h"

#include <cstdlib>
#include <iostream>

int main() {
  BstMap<int, int> bstmap;
  bstmap.add(1, 5);
  bstmap.add(3, 10);
  bstmap.add(-1, -5);
  bstmap.add(-2, -10);
  bstmap.add(0, 0);
  bstmap.add(6, 15);
  bstmap.add(5, 10);

  bstmap.inOrder();
  std::cout << "\n";

  bstmap.add(7, 200);
  bstmap.add(4, 100);

  BstMap<int, int> bstmap2(bstmap);  // copy
  std::cout << "MAP-2\n";
  bstmap2.inOrder();
  std::cout << "\n";

  bstmap.remove(1);  // root
  bstmap.remove(5);  // no child
  bstmap.inOrder();
  try {
    std::cout << bstmap.lookup(1) << std::endl;
  }
  catch (std::invalid_argument & ie) {
    std::cerr << "Caught exception: " << ie.what() << std::endl;
  }
  bstmap.inOrder();
  std::cout << "\n";
  bstmap.remove(3);  // one chile
  bstmap.inOrder();
  std::cout << "\n";
  bstmap.remove(4);  // one chile
  bstmap.inOrder();
  std::cout << "\n";
  bstmap.remove(6);  // one chile
  bstmap.inOrder();
  std::cout << "\n";

  bstmap2 = bstmap;  // copy
  std::cout << "MAP-2\n";
  bstmap2.inOrder();
  std::cout << "\n";

  return EXIT_SUCCESS;
}
